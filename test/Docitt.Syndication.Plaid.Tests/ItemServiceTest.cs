﻿using Docitt.Syndication.Plaid;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using RestSharp;
using Newtonsoft.Json;
using Microsoft.AspNet.Mvc;
using LendFoundry.Security.Tokens;
using LendFoundry.Security.Encryption;
using Docitt.Syndication.Plaid.Abstractions;

namespace LendFoundry.Configuration.Plaid.Tests
{
    public class ItemServiceTest : BaseService
    {
        public ItemServiceTest()
        {
            Logger = new Mock<ILogger>();
            ItemServiceMock = new Mock<IItemService>();
            PlaidClientService = new Mock<IPlaidClient>();
            PlaidConfiguration = new Mock<IPlaidConfiguration>();
            TenantTime = new Mock<ITenantTime>();
            InstitutionsRepository = new Mock<IInstitutionsRepository>();
            ItemRepository = new Mock<IItemRepository>();
            PlaidDelinkRepository = new Mock<IPlaidDelinkRepository>();
            TransactionRepository = new Mock<ITransactionRepository>();
            Configuration = new Mock<IConfiguration>();
            TokenReader = new Mock<ITokenReader>();
            TokenParser = new Mock<ITokenHandler>();
            WebhookRepository = new Mock<IWebhookRepository>();
            Configuration.Setup(x => x.PlaidConfiguration).Returns(new PlaidConfiguration() { ClientId = "test" });

            service = new ItemService(
                    Logger.Object,
                    PlaidClientService.Object,
                    TenantTime.Object,
                    Configuration.Object,
                    InstitutionsRepository.Object,
                    ItemRepository.Object,
                    TransactionRepository.Object,
                    WebhookRepository.Object,
                    PlaidDelinkRepository.Object,
                    TokenReader.Object,
                    TokenParser.Object
                );
           this.FillData();
        }

        private Mock<IPlaidDelinkRepository> PlaidDelinkRepository { get; }

        /// <summary>
        /// Gets ItemServiceMock
        /// </summary>
        private Mock<IItemService> ItemServiceMock { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private Mock<IPlaidClient> PlaidClientService { get; }

        private Mock<IPlaidConfiguration> PlaidConfiguration { get; }

        private Mock<ITenantTime> TenantTime { get; }

        private Mock<IInstitutionsRepository> InstitutionsRepository { get; }

        private Mock<IItemRepository> ItemRepository { get; }

        private Mock<ITransactionRepository> TransactionRepository { get; }

        private Mock<IConfiguration> Configuration { get; }

        private Mock<ILogger> Logger { get; }

        private Mock<ITokenHandler> TokenParser { get; }

        private Mock<IWebhookRepository> WebhookRepository { get; }

        /// <summary>
        /// Get TokenReader
        /// </summary>
        private Mock<ITokenReader> TokenReader { get; }

        private ItemService service { get; }

        //private IResponseItemCreate ItemCreateResponse;

        private IMFADocittRequest MfaDocittRequest;

        private IResponseItemCreate ResponseItemCreate;

        private PublicTokenExchangeResponse PublicTokenExchangeResponse;

        private IRequestItemDelete RequestItemDelete;

        private IPlaidResponseItemDelete PlaidResponseItemDelete;

        private IList<IItem> Items = new List<IItem>();

        private IList<AccountsVM> AccountResponseList = new List<AccountsVM>();

        private IResponseItemCreate ItemCreateResponseStatusForOne;

        private RequestItemCreate RequestItemCreate;

        [Fact]
        public async Task TestCreateItemAsync()
        {
            ItemRepository.Setup(x => x.GetItemStatus("17802", "ins_1")).Returns(ItemStatus.AccountExists);
            var result = await this.service.CreateItemAsync(this.RequestItemCreate);
            Assert.Equal(result.Error.DisplayMessage, this.ItemCreateResponseStatusForOne.Error.DisplayMessage);
        }

        [Fact]
        public async Task TestItemCreateMFAAsync()
        {
            this.PlaidClientService.Setup(x=>x.CreateMfaExecutionFlow(this.MfaDocittRequest))
                .Returns(Task.FromResult<IResponseItemCreate>(this.ResponseItemCreate));

            this.PlaidClientService.Setup(x => x.GetAccessTokenAsync("public-sandbox-6047ea24-b9b4-4aed-af6c-c885548fd96a"))
                .Returns(Task.FromResult<PublicTokenExchangeResponse>(this.PublicTokenExchangeResponse));

            var result = await this.service.ItemCreateMFAAsync(this.MfaDocittRequest);
            Assert.Equal(result, this.ResponseItemCreate);
        }

        [Fact]
        public async Task TestItemAccountDeleteAsync()
        {
            this.ItemRepository.Setup(x => x.RemoveAccountByAccountId("17802","asdf"))
                .Returns(true);

            var result = await this.service.ItemAccountDeleteAsync(this.RequestItemDelete);
            Assert.Equal(result, true);
        }

        [Fact]
        public async Task TestItemDeleteAsync()
        {
            var accesstoken = new string[]
            {
                "access-sandbox-6047ea24-b9b4-4aed-af6c-c885548fd96a"
            };
            this.ItemRepository.Setup(x => x.GetApplicantAllAccessToken("17802"))
               .Returns(accesstoken);
            this.ItemRepository.Setup(x => x.DeleteItemGivenAccessToken(accesstoken[0]))
               .Returns(true);
            this.PlaidClientService.Setup(x => x.ItemDeleteAsync(accesstoken))
                  .Returns(Task.FromResult<IList<PlaidResponseItemDelete>>(null)); //TODO
            
            var result = await this.service.ItemDeleteAsync(this.RequestItemDelete);
            Assert.Equal(result, null); //TODO
        }

        [Fact]
        public async Task TestGetItemsWithAccountsAsync()
        {
            this.ItemRepository.Setup(x => x.GetItemsByApplicantId("17802"))
                .Returns(this.Items);
            var result = await this.service.GetItemsWithAccounts("17802");
            Assert.Equal(result[0].PlaidItemId, this.AccountResponseList[0].PlaidItemId);
        }
        

        private void FillData()
        {
            this.MfaDocittRequest = JsonConvert.DeserializeObject<MFADocittRequest>(this.LoadJson("Item/ItemMfaRequest.json"));
            this.ResponseItemCreate = JsonConvert.DeserializeObject<ResponseItemCreate>(this.LoadJson("Item/ItemResponse.json"));
            this.PublicTokenExchangeResponse = JsonConvert.DeserializeObject<PublicTokenExchangeResponse>(this.LoadJson("Item/PublicTokenExchangeResponse.json"));
            this.RequestItemDelete = JsonConvert.DeserializeObject<RequestItemDelete>(this.LoadJson("Item/ItemDeleteRequest.json"));
            this.PlaidResponseItemDelete = JsonConvert.DeserializeObject<PlaidResponseItemDelete>(this.LoadJson("Item/ItemDeleteResponse.json"));
            var item = JsonConvert.DeserializeObject<Item>(this.LoadJson("Item/Items.json"));
            this.RequestItemCreate = JsonConvert.DeserializeObject<RequestItemCreate>(this.LoadJson("Item/ItemRequest.json"));
            Items.Add(item);
            this.AccountResponseList = JsonConvert.DeserializeObject<List<AccountsVM>>(this.LoadJson("Item/AccountResponse.json"));
            this.ItemCreateResponseStatusForOne = JsonConvert.DeserializeObject<ResponseItemCreate>(this.LoadJson("Item/ItemResponseStatusForOne.json"));
        }
    }
}
