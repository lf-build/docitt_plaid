﻿using Docitt.Syndication.Plaid;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Newtonsoft.Json;
using Docitt.Syndication.Plaid.Abstractions;

namespace LendFoundry.Configuration.Plaid.Tests
{
    public class InstitutionServiceTest : BaseService
    {
        public InstitutionServiceTest()
        {
            Logger = new Mock<ILogger>();
            PlaidClientService = new Mock<IPlaidClient>();
            PlaidConfiguration = new Mock<IPlaidConfiguration>();
            TenantTime = new Mock<ITenantTime>();
            FeatureInstitutionRepository = new Mock<IFeatureInstitutionsRepository>();
            InstitutionRepository = new Mock<IInstitutionsRepository>();
            Configuration = new Mock<IConfiguration>();

            Configuration.Setup(x => x.PlaidConfiguration).Returns(new PlaidConfiguration() { ClientId = "test" });

            service = new InstitutionService(
                    Logger.Object,
                    PlaidClientService.Object,
                    TenantTime.Object,
                    Configuration.Object,
                    FeatureInstitutionRepository.Object,
                    InstitutionRepository.Object
                );
            this.FillData();
        }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private Mock<IPlaidClient> PlaidClientService { get; }

        private Mock<IPlaidConfiguration> PlaidConfiguration { get; }

        private Mock<ITenantTime> TenantTime { get; }

        private Mock<IFeatureInstitutionsRepository> FeatureInstitutionRepository { get; }

        private Mock<IInstitutionsRepository> InstitutionRepository { get; }

        private Mock<IConfiguration> Configuration { get; }

        private Mock<ILogger> Logger { get; }

        private InstitutionService service { get; }

        private IInstitutions InstitutionResponse { get; set; }

        private IList<InstitutionsVM> InstitutionListResponse = new List<InstitutionsVM>();

        private List<IFeatureInstitutions> InstitutionRepoListResponse = new List<IFeatureInstitutions>();

        private InstitutionsVM InstitutionVMResponse { get; set; }

        [Fact]
        public async Task TestInstitutionsGetAsync()
        {
            PlaidClientService.Setup(x => x.InstitutionsGetAsync(2, 1))
                .Returns(Task.FromResult<IList<InstitutionsVM>>(this.InstitutionListResponse));
            var result = await service.InstitutionsGetAsync(2, 1);
            Assert.Same(result, this.InstitutionListResponse);
        }

        [Fact]
        public async Task TestInstitutionsSearchAsync()
        {
            PlaidClientService.Setup(x => x.InstitutionsSearchAsync("Bank"))
                .Returns(Task.FromResult<IList<InstitutionsVM>>(this.InstitutionListResponse));
            var result = await service.InstitutionsSearchAsync("Bank");
            Assert.Same(result, this.InstitutionListResponse);
        }

        [Fact]
        public async Task TestFeatureInstitutionsAsync()
        {
            FeatureInstitutionRepository.Setup(x => x.GetFeatureInstitutionsAsync())
                .ReturnsAsync((this.InstitutionRepoListResponse));
            var result = await service.GetFeatureInstitutionsAsync();
            Assert.Equal(result[0].PlaidInstitutionId, this.InstitutionListResponse[0].PlaidInstitutionId);
        }

        [Fact]
        public async Task TestInstitutionDetailsAsync()
        {
            InstitutionRepository.Setup(x => x.GetInstitutionByIdAsync("ins_2"))
                .ReturnsAsync((this.InstitutionResponse));
            var result = await service.InstitutionsDetailsAsync("ins_2");
            Assert.Same(result, this.InstitutionVMResponse);
        }

        private void FillData()
        {
            var institution1 = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/Institution1.json"));
            var institution2 = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/Institution2.json"));
            var institutionResponse = JsonConvert.DeserializeObject<FeatureInstitutions>(this.LoadJson("Institution/InstitutionRepoResponse.json"));
            this.InstitutionResponse = JsonConvert.DeserializeObject<Institutions>(this.LoadJson("Institution/InstitutionResponse.json"));
            this.InstitutionVMResponse = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/InstitutionVMResponse.json"));
            this.InstitutionListResponse.Add(institution1);
            this.InstitutionListResponse.Add(institution2);
            this.InstitutionRepoListResponse.Add(institutionResponse);
            this.InstitutionRepoListResponse.Add(institutionResponse);
        }
    }
}
