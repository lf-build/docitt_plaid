using Docitt.Syndication.Plaid.Client;
using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace Docitt.Syndication.Plaid.Api.Tests
{
    public class DccServiceClientTest
    {
        public DccServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            DccServiceClient = new DccPlaidService(MockServiceClient.Object);
        }

        private DccPlaidService DccServiceClient { get; }

        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        [Fact]
        public void GetFeatureInstitutions()
        {
            var returnVal = new List<IFeatureInstitutions>();
            var ins = new FeatureInstitutions() { PlaidInstitutionId = "123" };
            returnVal.Add(ins);

            MockServiceClient.Setup(s => s.Execute<List<IFeatureInstitutions>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () => returnVal
                );

            var result = DccServiceClient.GetFeatureInstitutions();
            Assert.Equal("institutions/feature", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }
    }
}
