﻿using Docitt.Syndication.Plaid;
using Docitt.Syndication.Plaid.Abstractions;
using Docitt.Syndication.Plaid.Api.Controllers;
using LendFoundry.Foundation.Logging;
using Microsoft.AspNet.Mvc;
using Moq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Configuration.Api.Tests
{
    public class ItemControllerTest : BaseService
    {
        public ItemControllerTest()
        {
            this.FillData();
            this.ItemDataService = new Mock<IItemService>();
        }

        private Mock<IItemService> ItemDataService { get;set; }

        private IResponseItemCreate ResponseItemCreate;

        private RequestItemCreate RequestItemCreate;

        private MFADocittRequest MfaRequest;

        private IResponseItemCreate ResponseMfaItem;

        private RequestItemDelete RequestItemDelete;

        private IPlaidResponseItemDelete ResponseItemDelete;

        private IList<AccountsVM> AccountResponseList = new List<AccountsVM>();

        private RequestItemAccount RequestItemAccount;

        private IAccountSyncVM AccountSyncVMResponse;

        private RequestItemUpdate RequestItemUpdate;

        private List<string> DelinkItemsResponse = new List<string>();

        private Mock<ILogger> Log { get; }

        [Fact]
        public async Task CreateItemTest()
        {
            ItemDataService.Setup(x => x.CreateItemAsync(this.RequestItemCreate))
                .Returns(Task.FromResult<IResponseItemCreate>(this.ResponseItemCreate));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.CreateItem(this.RequestItemCreate);
            var resultResponse = ((HttpOkObjectResult)result).Value as IResponseItemCreate;
            Assert.Same(resultResponse, this.ResponseItemCreate);
        }

        [Fact]
        public async Task MfaItemTest()
        {
            ItemDataService.Setup(x => x.ItemCreateMFAAsync(this.MfaRequest))
                .Returns(Task.FromResult<IResponseItemCreate>(this.ResponseItemCreate));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.MfaRequest(this.MfaRequest);
            var resultResponse = ((HttpOkObjectResult)result).Value as IResponseItemCreate;
            Assert.Same(resultResponse, this.ResponseMfaItem);
        }

        [Fact]
        public async Task DeleteItemTest()
        {
            ItemDataService.Setup(x => x.ItemDeleteAsync(this.RequestItemDelete))
                .Returns(Task.FromResult<IList<PlaidResponseItemDelete>>(null));//TO DO
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.DeleteItem(this.RequestItemDelete);
            var resultResponse = ((HttpOkObjectResult)result).Value as IPlaidResponseItemDelete;
            Assert.Same(resultResponse, this.ResponseItemDelete);
        }

        [Fact]
        public async Task DeleteItemAccountTest()
        {
            ItemDataService.Setup(x => x.ItemAccountDeleteAsync(this.RequestItemDelete))
                .Returns(Task.FromResult<bool>(true));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.DeleteItemAccount(this.RequestItemDelete);
            var resultResponse = ((HttpOkObjectResult)result).Value;
            Assert.Equal(resultResponse, true);
        }

        [Fact]
        public async Task GetItemAccountTest()
        {
            ItemDataService.Setup(x => x.GetItemsWithAccounts("12345"))
                .Returns(Task.FromResult<IList<AccountsVM>>(this.AccountResponseList));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.GetItemAccount("12345");
            var resultResponse = ((HttpOkObjectResult)result).Value as IList<AccountsVM>;
            Assert.Same(resultResponse, this.AccountResponseList);
        }

        [Fact]
        public async Task UpdateAccountSyncTest()
        {
            ItemDataService.Setup(x => x.GetAccountAsync(this.RequestItemAccount))
                .Returns(Task.FromResult<IAccountSyncVM>(this.AccountSyncVMResponse));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.UpdateAccountSync(this.RequestItemAccount);
            var resultResponse = ((HttpOkObjectResult)result).Value as IAccountSyncVM;
            Assert.Same(resultResponse, this.AccountSyncVMResponse);
        }

        [Fact]
        public async Task DelinkItemTest()
        {
            ItemDataService.Setup(x => x.DelinkItems(90))
                .Returns(Task.FromResult<List<string>>(this.DelinkItemsResponse));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.DelinkItems(90);
            var resultResponse = ((HttpOkObjectResult)result).Value as List<string>;
            Assert.Same(resultResponse, this.DelinkItemsResponse);
        }

        [Fact]
        public async Task ItemUpdateTest()
        {
            ItemDataService.Setup(x => x.ItemUpdateAsync(this.RequestItemUpdate))
                .Returns(Task.FromResult<IResponseItemCreate>(this.ResponseItemCreate));
            var controller = new ItemController(this.ItemDataService.Object, this.Log.Object);
            var result = await controller.UpdateItem(this.RequestItemUpdate);
            var resultResponse = ((HttpOkObjectResult)result).Value as IResponseItemCreate;
            Assert.Same(resultResponse, this.ResponseItemCreate);
        }

        private void FillData()
        {
            this.ResponseItemCreate = JsonConvert.DeserializeObject<ResponseItemCreate>(this.LoadJson("Item/ItemResponse.json"));
            this.RequestItemCreate = JsonConvert.DeserializeObject<RequestItemCreate>(this.LoadJson("Item/ItemRequest.json"));
            this.MfaRequest = JsonConvert.DeserializeObject<MFADocittRequest>(this.LoadJson("Item/ItemMfaRequest.json"));
            this.ResponseMfaItem = JsonConvert.DeserializeObject<ResponseItemCreate>(this.LoadJson("Item/ItemMfaResponse.json"));
            this.RequestItemDelete = JsonConvert.DeserializeObject<RequestItemDelete>(this.LoadJson("Item/ItemDeleteRequest.json"));
            this.ResponseItemDelete = JsonConvert.DeserializeObject<PlaidResponseItemDelete>(this.LoadJson("Item/ItemDeleteResponse.json"));
            this.AccountResponseList = JsonConvert.DeserializeObject<List<AccountsVM>>(this.LoadJson("Item/AccountResponse.json"));
            this.RequestItemAccount = JsonConvert.DeserializeObject<RequestItemAccount>(this.LoadJson("Item/RequestItemAccount.json"));
            this.AccountSyncVMResponse = JsonConvert.DeserializeObject<AccountSyncVM>(this.LoadJson("Item/AccountSyncResponse.json"));
            this.RequestItemUpdate = JsonConvert.DeserializeObject<RequestItemUpdate>(this.LoadJson("Item/ItemRequestUpdate.json"));
            this.DelinkItemsResponse.Add("aaaabbbb");
            this.DelinkItemsResponse.Add("ccccdddd");
        }
    }
}
