﻿using Docitt.Syndication.Plaid;
using Docitt.Syndication.Plaid.Abstractions;
using Docitt.Syndication.Plaid.Api.Controllers;
using Microsoft.AspNet.Mvc;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Configuration.Api.Tests
{
    public class TransactionControllerTest : BaseService
    {
        public TransactionControllerTest()
        {
            this.FillData();
            this.TransactionService = new Mock<ITransactionService>();
            this.FilterTransactionService = new Mock<IFilterTransactionService>();
            this.Controller = new TransactionController(this.TransactionService.Object, this.FilterTransactionService.Object);
        }

        private Mock<ITransactionService> TransactionService { get;set; }

        private Mock<IFilterTransactionService> FilterTransactionService { get; set; }

        private IList<ITransaction> TransactionList = new List<ITransaction>();

        private TransactionController Controller { get; }

        private RequestTransaction RequestTransaction;

        private Transaction Transaction;

        private TransactionResponse TransactionResponse;

        private IList<DateTime> DurationDates = new List<DateTime>();

        private FilterTransactionRequest FilterTransactionRequest;

        [Fact]
        public async Task GetTransactionsTest()
        {
            TransactionService.Setup(x => x.TransactionData(this.RequestTransaction))
                    .Returns(Task.FromResult<IEnumerable<ITransaction>>(TransactionList));
            var result = await this.Controller.GetTransactions(this.RequestTransaction);
            var resultResponse = ((HttpOkObjectResult)result).Value as IEnumerable<ITransaction>;
            Assert.Same(resultResponse, this.TransactionList);
        }

        [Fact]
        public async Task GetTransactionPullTest()
        {
            TransactionService.Setup(x => x.PullTransactionAsync(this.RequestTransaction))
                    .Returns(Task.FromResult<TransactionResponse>(this.TransactionResponse));
            var result = await this.Controller.GetTransactionPull(this.RequestTransaction);
            var resultResponse = ((HttpOkObjectResult)result).Value as TransactionResponse;
            Assert.Same(resultResponse, this.TransactionResponse);
        }

        [Fact]
        public async Task GetTransactionDurationTest()
        {
            TransactionService.Setup(x => x.TransactionDurationDates("aaaabbbbcccc"))
                    .Returns(Task.FromResult<IList<DateTime>>(this.DurationDates));
            var result = await this.Controller.GetTransactionDuration("aaaabbbbcccc");
            var resultResponse = ((HttpOkObjectResult)result).Value as IList<DateTime>;
            Assert.Same(resultResponse, this.DurationDates);
        }

        [Fact]
        public async Task FilterTransactionTest()
        {
            FilterTransactionService.Setup(x => x.GetFilterTransactions(this.FilterTransactionRequest))
                    .Returns(Task.FromResult<IList<ITransaction>>(this.TransactionList));
            var result = await this.Controller.FilterTransaction(this.FilterTransactionRequest);
            var resultResponse = ((HttpOkObjectResult)result).Value as IList<ITransaction>;
            Assert.Same(resultResponse, this.TransactionList);
        }

        private void FillData()
        {
            this.RequestTransaction = JsonConvert.DeserializeObject<RequestTransaction>(this.LoadJson("Transaction/RequestTransaction.json"));
            this.Transaction = JsonConvert.DeserializeObject<Transaction>(this.LoadJson("Transaction/Transaction.json"));
            this.TransactionResponse = JsonConvert.DeserializeObject<TransactionResponse>(this.LoadJson("Transaction/TransactionResponse.json"));
            this.FilterTransactionRequest = JsonConvert.DeserializeObject<FilterTransactionRequest>(this.LoadJson("Transaction/FilterTransactionRequest.json"));
            this.TransactionList.Add(Transaction);
            this.DurationDates.Add(DateTime.Now);
            this.DurationDates.Add(DateTime.Now.AddDays(10));
        }
    }
}
