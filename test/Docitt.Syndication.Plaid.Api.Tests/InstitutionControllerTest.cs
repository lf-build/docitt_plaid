using Docitt.Syndication.Plaid.Abstractions;
using Docitt.Syndication.Plaid.Api.Controllers;
using LendFoundry.Configuration.Api.Tests;
using LendFoundry.Foundation.Logging;
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.Caching.Memory;
using Moq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.Syndication.Plaid.Api.Tests
{
    public class InstitutionControllerTest : BaseService
    {
        public InstitutionControllerTest()
        {
            FillData();
            this.InstitutionService = new Mock<IInstitutionService>();
        }

        private List<InstitutionsVM> FeatureInstitutionListResponse = new List<InstitutionsVM>();

        private List<InstitutionsFeatureVM> FeatureInstitutionVMListResponse = new List<InstitutionsFeatureVM>();

        private List<InstitutionsSearchVM> SearchInstitutionVMListResponse = new List<InstitutionsSearchVM>();
        private Mock<IInstitutionService> InstitutionService { get; set; }

        private Mock<ILogger> Logger { get; }
        
        private IInstitutionsVM InstitutionVMResponse { get; set; }

        [Fact]
        public async Task GetFavouriteInstitutionsTest()
        {
            InstitutionService.Setup(x => x.GetFeatureInstitutionsAsync())
                .Returns(Task.FromResult<IList<InstitutionsFeatureVM>>(this.FeatureInstitutionVMListResponse));
            var controller = new InstitutionsController(
                this.InstitutionService.Object, 
                this.Logger.Object);
            var result = await controller.GetFeatureInstitutions();
            var resultResponse = ((HttpOkObjectResult)result).Value as List<InstitutionsVM>;
            Assert.Same(resultResponse, this.FeatureInstitutionListResponse);
        }

        [Fact]
        public async Task GetAllInstitutionsTest()
        {
            InstitutionService.Setup(x => x.InstitutionsGetAsync(2, 0))
                .Returns(Task.FromResult<IList<InstitutionsVM>>(this.FeatureInstitutionListResponse));
            var controller = new InstitutionsController(
                this.InstitutionService.Object, 
                this.Logger.Object);

            var result = await controller.GetInstitutions(2, 0);
            var resultResponse = ((HttpOkObjectResult)result).Value as List<InstitutionsVM>;
            Assert.Same(resultResponse, this.FeatureInstitutionListResponse);
        }

        [Fact]
        public async Task GetInstitutionDetailsTest()
        {
            InstitutionService.Setup(x => x.InstitutionsDetailsAsync("ins_2"))
                .Returns(Task.FromResult<IInstitutionsVM>(this.InstitutionVMResponse));
            var controller = new InstitutionsController(
                this.InstitutionService.Object, 
                this.Logger.Object);
            var result = await controller.GetInstitutionsWithDetails("ins_2");
            var resultResponse = ((HttpOkObjectResult)result).Value as IInstitutionsVM;
            Assert.Same(resultResponse, this.InstitutionVMResponse);
        }


        [Fact]
        public async Task GetSearchInstitutionsTest()
        {
            InstitutionService.Setup(x => x.InstitutionsSearchAsync("Bank"))
                .Returns(Task.FromResult<IList<InstitutionsSearchVM>>(this.SearchInstitutionVMListResponse));
            var controller = new InstitutionsController(
                this.InstitutionService.Object, 
                this.Logger.Object);

            var result = await controller.GetInstitutions("Bank",false);
            var resultResponse = ((HttpOkObjectResult)result).Value as List<InstitutionsVM>;
            Assert.Same(resultResponse, this.FeatureInstitutionListResponse);
        }

        private void FillData()
        {
            var institution1 = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/Institution1.json"));
            var institution2 = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/Institution2.json"));
            this.InstitutionVMResponse = JsonConvert.DeserializeObject<InstitutionsVM>(this.LoadJson("Institution/InstitutionVMResponse.json"));
            this.FeatureInstitutionListResponse.Add(institution1);
            this.FeatureInstitutionListResponse.Add(institution2);
        }

        
    }
}
