﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System;

namespace Docitt.Syndication.Plaid.Persistence
{
    /// <summary>
    /// EventLogRepository
    /// </summary>
    public class WebhookRepository : MongoRepository<IWebhookLog, WebhookLog>, IWebhookRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static WebhookRepository()
        {
            BsonClassMap.RegisterClassMap<WebhookLog>(map =>
            {
                map.AutoMap();
                map.MapProperty(x => x.ItemId).SetIgnoreIfDefault(true);
                map.MapProperty(x => x.Name).SetIgnoreIfDefault(true);
                map.MapProperty(x => x.NewTransactions).SetIgnoreIfDefault(true);
                map.MapProperty(x => x.RemovedTransactions).SetIgnoreIfDefault(true);
                map.MapProperty(x => x.WebhookCode).SetIgnoreIfDefault(true);
                map.MapProperty(x => x.WebhookType).SetIgnoreIfDefault(true);
                var type = typeof(WebhookLog);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebhookRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public WebhookRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "webhooklog")
        {
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        public bool AddWebhookLog(IWebhookLog webhookData)
        {
            try
            {
                webhookData.TenantId = TenantService.Current.Id;
                Collection.InsertOne(webhookData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}