﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Linq;
using Docitt.Syndication.Plaid.Abstractions;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class InstitutionsRepository :
        MongoRepository<IInstitutions, Institutions>, IInstitutionsRepository
    {
        static InstitutionsRepository()
        {
            BsonClassMap.RegisterClassMap<Institutions>(map =>
            {
                map.AutoMap();
                
                var type = typeof(Institutions);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public InstitutionsRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "institutions")
        {
            //CreateIndexIfNotExists("unique_key", Builders<IInstitutions>.IndexKeys
            //    .Ascending(institution => institution.PlaidInstitutionId), true);
        }

        /// <summary>
        /// GetInstitutions
        /// </summary>
        /// <returns>Institution Collection</returns>
        public IList<IInstitutions> GetInstitutions()
        {
            return Collection
                    .AsQueryable<IInstitutions>()
                    .ToList();
        }

        /// <summary>
        /// GetInstitutionsBySearch
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IList<IInstitutions> GetInstitutionsBySearch(string search)
        {
            return Collection
                .Find(x => 
                            x.Products.Contains("transactions") && 
                            x.Products.Contains("auth") && 
                            x.InstitutionName.ToLower().Contains(search.ToLower())
                     ).ToList();
        }

        /// <summary>
        /// GetInstitutionsBySearch
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IList<IInstitutions> GetInstitutionsSearchList()
        {
            return Collection.Find(x => x.Products.Contains("transactions") && x.Products.Contains("auth")).ToList();
        }

        /// <summary>
        /// GetInstitutionIds
        /// </summary>
        /// <returns>Institution ids Collection</returns>
        public IList<string> GetInstitutionIds()
        {
            return Collection
                    .AsQueryable<IInstitutions>()
                    .Select(institution => institution.PlaidInstitutionId)
                    .ToList();
        }

        /// <summary>
        /// GetInstitutionById
        /// </summary>
        /// <param name="institutionId">institutionId</param>
        /// <returns>Institutions</returns>
        public async Task<IInstitutions> GetInstitutionByIdAsync(string institutionId)
        {
            var result = await Collection
                .FindAsync(inst => inst.PlaidInstitutionId == institutionId);
                        
            return result.FirstOrDefault();
        }

        /// <summary>
        /// AddInstitutions
        /// </summary>
        /// <param name="institutions">institutions</param>
        /// <returns>string</returns>
        public string AddInstitutions(IEnumerable<IInstitutions> institutions)
        {
            //foreach (var institution in institutions)
            //{
            //    institution.TenantId = TenantService.Current.Id;
            //}

            Collection.InsertMany(institutions);

            return "Institutions added successfully.";
        }

        /// <summary>
        /// AddInstitution
        /// </summary>
        /// <param name="institution">institution</param>
        /// <returns>string</returns>
        public string AddInstitution(IInstitutions institution)
        {
           // institution.TenantId = TenantService.Current.Id;
            Collection.InsertOne(institution);

            return "Institution added successfully.";
        }

        /// <summary>
        /// UpdateInstitution
        /// </summary>
        /// <param name="institution">institution</param>
        /// <returns>string</returns>
        public string UpdateInstitution(IInstitutions institution)
        {
            Collection.UpdateOne(
                    Builders<IInstitutions>.Filter
                        .Where(objInstitution => objInstitution.PlaidInstitutionId == institution.PlaidInstitutionId),
                    Builders<IInstitutions>.Update
                        .Set(objInstitution => objInstitution.InstitutionLogo, institution.InstitutionLogo)
                        .Set(objInstitution => objInstitution.UrlAccountLocked, institution.UrlAccountLocked)
                        .Set(objInstitution => objInstitution.UrlAccountSetup, institution.UrlAccountSetup)
                        .Set(objInstitution => objInstitution.UrlForgottenPassword, institution.UrlForgottenPassword)
                        .Set(objInstitution => objInstitution.Colors, institution.Colors)
            );
            return "Institution updated successfully.";
        }

        /// <summary>
        /// RemoveInstitutionByAccount
        /// </summary>
        /// <param name="institutionIds">institutionIds</param>
        /// <returns>bool</returns>
        public bool RemoveInstitutionByInstitution(List<string> institutionIds)
        {
            Collection.DeleteMany(
                    Builders<IInstitutions>.Filter
                        .Where(institution => institutionIds.Contains(institution.PlaidInstitutionId))
                );
            return true;
        }
    }
}