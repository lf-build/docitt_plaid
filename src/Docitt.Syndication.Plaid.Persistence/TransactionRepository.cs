﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Docitt.Syndication.Plaid.Persistence
{
    /// <summary>
    /// TransactionRepository
    /// </summary>
    public class TransactionRepository : MongoRepository<ITransaction, Transaction>, ITransactionRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static TransactionRepository()
        {
            BsonClassMap.RegisterClassMap<Transaction>(map =>
            {
                map.AutoMap();
                // map.MapProperty(p => p.AccountId).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.PlaidAccountId).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.PlaidTransactionId).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.Amount).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.Category).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.CategoryId).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.Type).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.Date).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.Pending).SetIgnoreIfDefault(true);
                map.MapProperty(transaction => transaction.EntityId).SetIgnoreIfDefault(true);
                var type = typeof(Transaction);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public TransactionRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "transaction")
        {
            CreateIndexIfNotExists("tenant_id", Builders<ITransaction>.IndexKeys
                .Ascending(transaction => transaction.TenantId), false);
            CreateIndexIfNotExists("unique_key", Builders<ITransaction>.IndexKeys
                .Ascending(transaction => transaction.TenantId)
                .Ascending(transaction => transaction.EntityId)
                .Ascending(transaction => transaction.PlaidAccountId)
                .Ascending(transaction => transaction.PlaidTransactionId), true);
        }

        /// <summary>
        /// GetAllTransactions
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>Transaction Collection</returns>
        public IEnumerable<ITransaction> GetAllTransactions(string accountId, string entityId = "")
        {
            var transactionQuery = Query;
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                transactionQuery = transactionQuery.Where(transaction => transaction.EntityId == entityId);
            }
            transactionQuery = transactionQuery.Where(transaction => transaction.PlaidAccountId == accountId);
            return transactionQuery.ToList();
        }

        /// <summary>
        /// GetTransactionsByDateRange
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <param name="entityId">entityId</param>
        /// <returns>Transaction Collection</returns>
        public IList<ITransaction> GetTransactions(IRequestTransaction transactionRequest, string entityId = "")
        {
            var startDate = Convert.ToDateTime(transactionRequest.StartDate);
            var endDate = Convert.ToDateTime(transactionRequest.EndDate).AddSeconds(86400);

            var transactionQuery = Query;

            if (!string.IsNullOrWhiteSpace(entityId))
            {
                transactionQuery = transactionQuery.Where(transaction => transaction.EntityId == entityId);
            }
            transactionQuery = transactionQuery.Where(transaction => transaction.PlaidAccountId == transactionRequest.PlaidAccountId
                        && transaction.Date >= startDate.ToUniversalTime()
                        && transaction.Date <= endDate.ToUniversalTime())
                        .OrderByDescending(transaction => transaction.Date)
                        .Skip(transactionRequest.Offset - 1);

            if (transactionRequest.Count > 0)
            {
                transactionQuery = transactionQuery.Take(transactionRequest.Count);
            }

            return transactionQuery.ToList();
        }

        /// <summary>
        /// GetTransactionStartEndDates
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>Transaction dates array</returns>
        public List<DateTime> GetTransactionStartEndDates(string accountId, string entityId = "")
        {
            var builder = Builders<ITransaction>.Filter;
            var filter = builder.Eq("TenantId", TenantService.Current.Id);
            if (!string.IsNullOrEmpty(entityId))
            {
                filter = filter & builder.Eq("EntityId", entityId);
            }
            filter = filter & builder.Eq("PlaidAccountId", accountId);

            var minDate = Collection.Find(filter)
                .Sort(Builders<ITransaction>.Sort.Ascending("Date")).First();

            var maxDate = Collection.Find(filter)
                .Sort(Builders<ITransaction>.Sort.Descending("Date")).First();

            return new List<DateTime>()
            {
                minDate.Date,
                maxDate.Date
            };
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <param name="entityId">entityId</param>
        /// <returns>string</returns>
        public string AddTransaction(ITransaction transaction, string entityId = "")
        {
            // Check if data exists
            var objTransaction = Query
                 .Where(trans => trans.PlaidTransactionId == transaction.PlaidTransactionId)
                 .SingleOrDefault();

            if (objTransaction == null)
            {
                transaction.TenantId = TenantService.Current.Id;
                transaction.EntityId = entityId;
                Collection.InsertOne(transaction);
            }

            return "Transaction added successfully.";
        }

        /// <summary>
        /// AddTransactions
        /// </summary>
        /// <param name="transactions">transactions</param>
        /// <param name="entityId">entityId</param>
        /// <returns>string</returns>
        public string AddTransactions(IEnumerable<ITransaction> transactions, string entityId = "")
        {
            foreach (var transaction in transactions)
            {
                transaction.TenantId = TenantService.Current.Id;
                transaction.EntityId = entityId;
            }

            try
            {
                Collection.InsertMany(transactions);
            }
            catch (MongoBulkWriteException)
            {
                foreach (var transaction in transactions)
                {
                    AddTransaction(transaction, entityId);
                }
            }

            return "Transaction added successfully.";
        }

        /// <summary>
        /// UpdateTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        public string UpdateTransaction(ITransaction transaction)
        {
            Collection.UpdateOne(
                    Builders<ITransaction>.Filter
                        .Where(objTransaction => objTransaction.TenantId == TenantService.Current.Id
                        && objTransaction.Id == transaction.Id),
                    Builders<ITransaction>.Update
                        .Set(objTransaction => objTransaction.PlaidAccountId, transaction.PlaidAccountId)
                        .Set(objTransaction => objTransaction.PlaidAccountId, transaction.PlaidAccountId)
                        .Set(objTransaction => objTransaction.PlaidTransactionId, transaction.PlaidTransactionId)
                        .Set(objTransaction => objTransaction.Amount, transaction.Amount)
                        .Set(objTransaction => objTransaction.Category, transaction.Category)
                        .Set(objTransaction => objTransaction.CategoryId, transaction.CategoryId)
                        .Set(objTransaction => objTransaction.Type, transaction.Type)
                        .Set(objTransaction => objTransaction.Date, transaction.Date)
                        .Set(objTransaction => objTransaction.Pending, transaction.Pending)
            );
            return "Transaction updated successfully.";
        }

        /// <summary>
        /// RemoveTransaction
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>bool</returns>
        public bool RemoveTransactionByAccount(List<string> plaidAccountId, string entityId = "")
        {
            var builder = Builders<ITransaction>.Filter;
            var filter = builder.Eq("TenantId", TenantService.Current.Id);
            if (!string.IsNullOrEmpty(entityId))
            {
                filter = filter & builder.Eq("EntityId", entityId);
            }
            filter = filter & builder.AnyEq("PlaidAccountId", plaidAccountId);
            Collection.DeleteMany(filter);
            return true;
        }

        /// <summary>
        /// GetFilterTransactionData
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <param name="transactionType">transactionType</param>
        /// <param name="transactionAmount">transactionAmount</param>
        /// <returns>list of transactions</returns>
        public IList<ITransaction> GetFilterTransactionData(IFilterTransactionRequest request)
        {
            var builder = Builders<ITransaction>.Filter;
            var filter = builder.Eq("TenantId", TenantService.Current.Id);
            filter = filter & builder.Where(transaction => request.AccountId.Contains(transaction.PlaidAccountId));
            request.EndDate = Convert.ToDateTime(request.EndDate).AddSeconds(86400);

            filter = filter & builder.Gte("Date", request.StartDate.ToUniversalTime());
            filter = filter & builder.Lte("Date", request.EndDate.ToUniversalTime());
            if(!string.IsNullOrWhiteSpace(request.EntityId))
            {
                filter = filter & builder.Eq("EntityId", request.EntityId);
            }
            if (request.PlaidCategoryIds != null)
            {
                filter = filter & builder.Where(transaction => !request.PlaidCategoryIds.Contains(transaction.CategoryId) || transaction.Type.Contains("unresolved"));
            }

            if (request.TransactionType.ToLower().Equals("cr"))
            {
                switch (request.Comparer)
                {
                    case "<":
                        filter = filter & builder.Gt("Amount", -request.TransactionAmount);
                        break;
                    case ">":
                        filter = filter & builder.Lt("Amount", -request.TransactionAmount);
                        break;
                    case "<=":
                        filter = filter & builder.Gte("Amount", -request.TransactionAmount);
                        break;
                    case ">=":
                        filter = filter & builder.Lte("Amount", -request.TransactionAmount);
                        break;
                    default:
                        filter = filter & builder.Eq("Amount", -request.TransactionAmount);
                        break;
                }
            }
            else
            {
                switch (request.Comparer)
                {
                    case "<":
                        filter = filter & builder.Lt("Amount", request.TransactionAmount);
                        break;
                    case ">":
                        filter = filter & builder.Gt("Amount", request.TransactionAmount);
                        break;
                    case "<=":
                        filter = filter & builder.Lte("Amount", request.TransactionAmount);
                        break;
                    case ">=":
                        filter = filter & builder.Gte("Amount", request.TransactionAmount);
                        break;
                    default:
                        filter = filter & builder.Eq("Amount", request.TransactionAmount);
                        break;
                }
            }

            var transactions = Collection.Find(filter).ToList();

            return transactions;
        }
    }
}