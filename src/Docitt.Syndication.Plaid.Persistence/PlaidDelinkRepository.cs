﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid
{
    public class PlaidDelinkRepository :
        MongoRepository<IPlaidDelink, PlaidDelink>, IPlaidDelinkRepository
    {
        static PlaidDelinkRepository()
        {
            BsonClassMap.RegisterClassMap<PlaidDelink>(map =>
            {
                map.AutoMap();
                
                var type = typeof(PlaidDelink);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public PlaidDelinkRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "plaiddelink")
        {
          
        }

    }
}