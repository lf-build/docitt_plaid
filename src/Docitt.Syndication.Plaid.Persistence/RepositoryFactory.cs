﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Tenant.Client;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Encryption;

namespace Docitt.Syndication.Plaid.Persistence
{
    public interface IRepositoryFactory
    {
        IItemRepository CreateItemRepository(ITokenReader reader);

        IAssetReportRepository CreateAssetReportRepository(ITokenReader reader);

        ITransactionRepository CreateTransactionRepository(ITokenReader reader);

        IInstitutionsRepository CreateInstitutionsRepository(ITokenReader reader);

        IWebhookRepository CreateWebhookRepository(ITokenReader reader);

        IPlaidDelinkRepository CreatePlaidDelinkRepository(ITokenReader reader);
    }

    public class RepositoryFactory : IRepositoryFactory
    {
        public RepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IItemRepository CreateItemRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new ItemRepository(tenantService, mongoConfiguration);
        }

        public IAssetReportRepository CreateAssetReportRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());

            var encrypterService = Provider.GetService<IEncryptionService>();
            return new AssetReportRepository(tenantService, mongoConfiguration, encrypterService);
        }

        public ITransactionRepository CreateTransactionRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new TransactionRepository(tenantService, mongoConfiguration);
        }

        public IInstitutionsRepository CreateInstitutionsRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new InstitutionsRepository(tenantService, mongoConfiguration);
        }

        public IWebhookRepository CreateWebhookRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new WebhookRepository(tenantService, mongoConfiguration);
        }

        public IPlaidDelinkRepository CreatePlaidDelinkRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new PlaidDelinkRepository(tenantService, mongoConfiguration);
        }
    }
}
