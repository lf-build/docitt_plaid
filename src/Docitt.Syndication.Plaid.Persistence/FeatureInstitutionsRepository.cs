﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Persistence
{
    public class FeatureInstitutionsRepository : 
        MongoRepository<IFeatureInstitutions, FeatureInstitutions> , IFeatureInstitutionsRepository
    {
        static FeatureInstitutionsRepository()
        {
            BsonClassMap.RegisterClassMap<FeatureInstitutions>(map =>
            {
                map.AutoMap();
                map.MapProperty(institution => institution.InstitutionName);
                map.MapProperty(institution => institution.PlaidInstitutionId).SetIgnoreIfDefault(true);
                map.MapProperty(institution => institution.InstitutionName).SetIgnoreIfDefault(true);
                map.MapProperty(institution => institution.Credentials).SetIgnoreIfDefault(true);
                map.MapProperty(institution => institution.HasMFA).SetIgnoreIfDefault(true);
                map.MapProperty(institution => institution.SortOrder).SetIgnoreIfDefault(true);
                var type = typeof(FeatureInstitutions);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FeatureInstitutionsRepository(ITenantService tenantService, IMongoConfiguration configuration) 
            : base(tenantService, configuration, "featureinstitutions")
        {
            
        }

        public async Task<List<IFeatureInstitutions>> GetFeatureInstitutionsAsync()
        {
            return await Query
                    .Where<IFeatureInstitutions>(x => 1 == 1)
                    .ToListAsync(); 
        }
    }
}