using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System;
using Docitt.Syndication.Plaid.Abstractions;
using System.Threading.Tasks;
using LendFoundry.Security.Encryption;

namespace Docitt.Syndication.Plaid.Persistence
{
    /// <summary>
    /// AssetReportRepository
    /// </summary>
    public class AssetReportRepository : MongoRepository<IAssetReport, AssetReport>, IAssetReportRepository
    {
        static AssetReportRepository() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public AssetReportRepository(
            ITenantService tenantService, 
            IMongoConfiguration configuration,
            IEncryptionService encrypterService) : base(tenantService, configuration, "assetreport")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(AssetReport)))
            {
                BsonClassMap.RegisterClassMap<AssetReport>(map =>
                {
                    map.AutoMap();
                    map.MapProperty(item => item.ApplicantId).SetIgnoreIfDefault(true);
                    map.MapProperty(item => item.AssetReportId).SetIgnoreIfDefault(true);
                    //encrypt 
                    map.MapProperty(item => item.AssetReportToken).SetSerializer(new BsonEncryptor<string, string>(encrypterService));;
                    map.MapProperty(item => item.RequestId).SetIgnoreIfDefault(true);
                    map.MapProperty(item => item.CreatedDateTime).SetIgnoreIfDefault(true);
                    map.MapProperty(item => item.UpdatedDateTime).SetIgnoreIfDefault(true);

                    var type = typeof(AssetReport);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }

            CreateIndexIfNotExists("tenant_id", Builders<IAssetReport>.IndexKeys
                .Ascending(item => item.TenantId), false);

            CreateIndexIfNotExists("applicant_id", Builders<IAssetReport>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.ApplicantId), false);

            CreateIndexIfNotExists("asset_report_token", Builders<IAssetReport>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.AssetReportToken), false);
        }

        /// <summary>
        /// Add a new asset report entry
        /// </summary>
        /// <param name="IAssetReport">data</param>
        /// <returns>string</returns>
        public string AddAssetReport(IAssetReport data)
        {
            //// Update TenantId 
            data.TenantId = TenantService.Current.Id;

            //// Store asset report information in database 
            Collection
                .InsertOne(data);

            return "AssetReport added successfully.";
        }

        /// <summary>
        /// GetAssetReportTokenGivenApplicationId
        /// </summary>
        /// <param name="ApplicantId">Application Id</param>
        /// <returns></returns>
        public Task<string> GetAssetReportTokenGivenApplicationId(string applicantId)
        {
            var result = Query
                    .Where(x => x.TenantId.Equals(TenantService.Current.Id)
                            && x.ApplicantId.Equals(applicantId))
                    .Select(x=>x.AssetReportToken)
                    .FirstOrDefaultAsync();
                    
            return result;
        }

        /// <summary>
        /// GetAssetReportDataByApplicationId
        /// </summary>
        /// <param name="ApplicantId">Application Id</param>
        /// <returns></returns>
        public Task<IAssetReport> GetAssetReportDataByApplicationId(string applicantId)
        {
            var result = Query
                    .Where(x => x.TenantId.Equals(TenantService.Current.Id)
                            && x.ApplicantId.Equals(applicantId))
                            .FirstOrDefaultAsync();

            
            return result;
        }

        /// <summary>
        /// DeleteAssetReportTokenGivenApplicationId
        /// </summary>
        /// <param name="ApplicantId">Applicant Id</param>
        /// <returns></returns>
        public bool DeleteAssetReportTokenGivenApplicationId(string ApplicantId)
        {
            var result = Collection
                .DeleteOne(Builders<IAssetReport>.Filter
                            .Where(x => x.TenantId.Equals(TenantService.Current.Id) && x.ApplicantId.Equals(ApplicantId)));

            return result.IsAcknowledged;
        }

        /// <summary>
        /// GetAssetReportTokenGivenApplicationId
        /// </summary>
        /// <param name="ApplicantId">Application Id</param>
        /// <returns></returns>
        public Task<IAssetReport> GetAssetReportGivenAssetReportId(string assetReportId)
        {
            var result = Query
                    .Where(x => x.TenantId.Equals(TenantService.Current.Id)
                            && x.AssetReportId.Equals(assetReportId))
                    .Select(x=>x)
                    .FirstOrDefaultAsync();
                    
            return result;
        }

        public bool UpdateAssetReportDataGivenApplicantId(string applicantId, IResponseAssetReportData data)
        {
             UpdateResult updateResult = Collection.UpdateOne(Builders<IAssetReport>
                                        .Filter
                                        .Where(x => x.TenantId.Equals(TenantService.Current.Id)
                                                && x.ApplicantId.Equals(applicantId)),
                                       Builders<IAssetReport>
                                        .Update
                                        .Set(objItem => objItem.Data, Newtonsoft.Json.JsonConvert.SerializeObject(data)));

            return updateResult.IsAcknowledged;
            
        }

        public bool UpdateAssetReportDataGivenAssetReportId(string assetReportId, IResponseAssetReportData data)
        {
             UpdateResult updateResult = Collection.UpdateOne(Builders<IAssetReport>
                                        .Filter
                                        .Where(x => x.TenantId.Equals(TenantService.Current.Id)
                                                && x.AssetReportId.Equals(assetReportId)),
                                       Builders<IAssetReport>
                                        .Update
                                        .Set(objItem => objItem.Data, Newtonsoft.Json.JsonConvert.SerializeObject(data)));

            return updateResult.IsAcknowledged; 
        }
    }
}