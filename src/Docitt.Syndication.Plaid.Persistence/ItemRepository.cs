﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System;
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid.Persistence
{
    /// <summary>
    /// ItemRepository
    /// </summary>
    public class ItemRepository : MongoRepository<IItem, Item>, IItemRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static ItemRepository()
        {
            BsonClassMap.RegisterClassMap<Item>(map =>
            {
                map.AutoMap();
                map.MapProperty(item => item.ApplicantId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidItemId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidInstitutionId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidPublicToken).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidAccessToken).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.CreatedDateTime).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.EntityId).SetIgnoreIfDefault(true);
                var type = typeof(Item);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public ItemRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "item")
        {
            CreateIndexIfNotExists("tenant_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId), false);

            CreateIndexIfNotExists("applicant_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.ApplicantId), false);

            CreateIndexIfNotExists("plaid_item_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.PlaidItemId), false);

            CreateIndexIfNotExists("entity_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.EntityId)
                .Ascending(item => item.ApplicantId), false);
        }

        /// <summary>
        /// GetItemByApplicantId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>Item Collection</returns>
        public IList<IItem> GetItemsByApplicantId(string applicant_Id, string entityId = "")
        {
            var delItemList = new List<IItem>();

            var itemQuery = Query;
            if(!string.IsNullOrWhiteSpace(entityId))
            {
                itemQuery = itemQuery.Where(x => x.EntityId == entityId);
            }
            itemQuery = itemQuery.Where(item => item.ApplicantId.Equals(applicant_Id));

            var itemList = itemQuery.ToList();
            //// Filter data set so not to display accounts marked as isLinked
            foreach (var item in itemList)
            {
                //// Hide all accounts which are marked as IsLinked = false 
                item.Accounts.RemoveAll(account => account.IsLinked == false);

                //// Dont display items which doesn't have any accounts 
                if (item.Accounts.Count == 0)
                    delItemList.Add(item);

            }

            foreach (var delItem in delItemList)
                itemList.Remove(delItem);

            return itemList;
        }

        /// <summary>
        /// GetAccountsInfoByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>accounts</returns>
        public IList<Accounts> GetAccountsInfoByItemId(string itemId)
        {
            var accountList = new List<Accounts>();

            Query
                .Where(account => account.PlaidItemId.Equals(itemId))
                .SingleOrDefault()
                .Accounts
                    .ForEach(account =>
                    {
                        if (account.IsLinked)
                        {
                            accountList.Add(account);
                        }
                    });

            return accountList;
        }

        /// <summary>
        /// GetApplicantAllAccessToken
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>All access token</returns>
        public IList<string> GetApplicantAllAccessToken(string applicantId, string entityId = "")
        {
            var accessTokenQuery = Query;

            if(!string.IsNullOrWhiteSpace(entityId))
            {
                accessTokenQuery = accessTokenQuery.Where(item => item.EntityId == entityId);
            }
            accessTokenQuery = accessTokenQuery.Where(item => item.ApplicantId == applicantId 
                                && item.PlaidAccessToken != null);

            return accessTokenQuery.Select(item => item.PlaidAccessToken).ToList();
        }

        /// <summary>
        /// GetAccountsByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>All accounts</returns>
        public IEnumerable<string> GetAccountsByItemId(string itemId)
        {
            List<string> strAccounts = new List<string>();
            Query
                .Where(item => item.PlaidItemId == itemId)
                .FirstOrDefault().Accounts.ForEach(account =>
                {
                    if (account.IsLinked)
                        strAccounts.Add(account.PlaidAccountId);
                });

            return strAccounts;
        }

        /// <summary>
        /// GetAccountsByApplicantId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>All accounts</returns>
        public IList<string> GetAccountsByApplicantId(string applicantId, string entityId = "")
        {
            var accountStringList = new List<string>();
            var accountQuery = Query;
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                accountQuery = accountQuery.Where(item => item.EntityId == entityId);
            }
            accountQuery = accountQuery.Where(item => item.ApplicantId == applicantId);
            accountQuery.ToList().ForEach(
                                        item => item.Accounts
                                        .ForEach(account => { if (account.IsLinked) { accountStringList.Add(account.PlaidAccountId); } })
                                    );            
            return accountStringList;
        }

        /// <summary>
        /// GetAccessTokenByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>access token</returns>
        public string GetAccessTokenByItemId(string itemId)
        {
            return Query
                .Where(item => item.PlaidItemId.Equals(itemId))
                .Select(item => item.PlaidAccessToken)
                .FirstOrDefault();
        }

        /// <summary>
        /// GetItemStatus
        /// </summary>
        /// <param name="applicant_Id">applicantId</param>
        /// <param name="institution_Id">institutionId</param>
        /// <param name="entity_Id">entityId</param>
        /// <returns>-1 if item not exists, 1 if item and account exists, 0 if item exist but all accounts unlinked</returns>
        public ItemStatus GetItemStatus(string applicant_Id, string institution_Id, string entity_id = "")
        {
            var itemQuery = Query;

            if (!string.IsNullOrWhiteSpace(entity_id))
            {
                itemQuery = itemQuery.Where(item => item.EntityId == entity_id);
            }
            itemQuery = itemQuery.Where(item => item.ApplicantId.Equals(applicant_Id)
                                  && item.PlaidInstitutionId.Equals(institution_Id));
                
            var objItem = itemQuery.FirstOrDefault();
            if (objItem == null || string.IsNullOrWhiteSpace(objItem.Id))
            {
                return ItemStatus.NoRecordsFound; //// No Records Found
            }
            objItem.Accounts.RemoveAll(account => account.IsLinked == false);

            if (objItem.Accounts.Count > 0)
            {
                return ItemStatus.AccountExists;
            }

            return ItemStatus.AccountNotExists;
        }

        /// <summary>
        /// GetItemIdByAccessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>Item id</returns>
        public string GetItemIdByAccessToken(string accessToken)
        {
            return Query
                .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                        && item.PlaidAccessToken.Equals(accessToken))
                .Select(item => item.PlaidItemId)
                .FirstOrDefault();
        }

        /// <summary>
        /// Get Item details given applicant_Id and institution_Id
        /// </summary>
        /// <param name="applicant_Id">applicantId</param>
        /// <param name="institution_Id">institutionId</param>
        /// <param name="entity_Id">entityId</param>
        /// <returns>Item Data</returns>
        public IItem GetItemGivenApplicantIdInstitutionId(string applicant_Id, string institution_Id, string entity_id = "")
        {
            var itemQuery = Query;

            if (!string.IsNullOrWhiteSpace(entity_id))
            {
                itemQuery = itemQuery.Where(item => item.EntityId == entity_id);
            }
            itemQuery = itemQuery.Where(item => item.ApplicantId == applicant_Id
                                && item.PlaidInstitutionId == institution_Id
                                && item.PlaidAccessToken != null);

            return itemQuery.FirstOrDefault();
        }

        /// <summary>
        /// GetItemByAccountId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>return Item</returns>
        public IItem GetItemByAccountId(string accountId)
        {
            var builder = Builders<IItem>.Filter;

            var filter = builder.Eq("TenantId", TenantService.Current.Id) 
                        & builder.Eq("Accounts.PlaidAccountId", accountId);

            return Collection.Find(filter).FirstOrDefault<IItem>();
        }


        /// <summary>
        /// Get Item details given item_id
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>Item Data</returns>
        public IItem GetItemByGivenItemId(string item_id)
        {
            var result = Query
                    .Where(item => item.PlaidItemId.Equals(item_id))
                    .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Add a new item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public string AddItem(IItem item)
        {
            //// Update TenantId 
            item.TenantId = TenantService.Current.Id;

            //// Store item in database 
            Collection
                .InsertOne(item);

            return "Item added successfully.";
        }

        /// <summary>
        /// Hard Delete Item Information given itemId
        /// </summary>
        /// <param name="item_id">itemId</param>
        /// <returns>true or false</returns>
        public bool DeleteItemGivenItemId(string item_id)
        {
            var result = Collection.DeleteOne(Builders<IItem>
                                                .Filter
                                                .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                                        && item.PlaidItemId.Equals(item_id)));

            //TODO: Delete transaction information 

            return result.IsAcknowledged;
        }

        /// <summary>
        /// Hard Delete Item Information given accessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>true or false</returns>
        public bool DeleteItemGivenAccessToken(string accessToken)
        {
            var result = Collection
                            .DeleteOne(Builders<IItem>
                                        .Filter
                                        .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                                && item.PlaidAccessToken.Equals(accessToken)));
            // TODO: Delete transaction information 

            return result.IsAcknowledged;
        }

        /// <summary>
        /// RemoveAccountByAccountId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="accountId">accountId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>true or false</returns>
        public bool RemoveAccountByAccountId(string applicantId, string accountId, string entityId = "")
        {
            var builder = Builders<IItem>.Filter;
            var filter = builder.Eq("TenantId", TenantService.Current.Id);
            if(!string.IsNullOrEmpty(entityId))
            {
                filter = filter & builder.Eq("EntityId", entityId);
            }
            filter = filter & builder.Eq("Accounts.PlaidAccountId", accountId);

            //// Get the list of items linked for given applicant
            var objItems = Collection
                            .Find(filter)
                            .ToList();

            if(objItems != null && objItems.Count > 0  && !string.IsNullOrWhiteSpace(entityId))
            {
                objItems = objItems.Where(x => x.EntityId == entityId).ToList();
            }

            var objItem = objItems.FirstOrDefault();

            if (objItem == null)
                throw new NotFoundException($"Item {objItem} cannot be found");

            //// locate the account using accountId and update the balances
            var accounts = objItem.Accounts;
            var accountToUpdate = accounts.Find(account => account.PlaidAccountId == accountId);

            if (accountToUpdate == null)
                throw new NotFoundException($"Account {accountToUpdate} not found");

            accountToUpdate.IsLinked = false;

            //// Update information in database
            UpdateResult updateResult = Collection.UpdateOne
            (
                Builders<IItem>.Filter
                    .Where(item => item.TenantId == TenantService.Current.Id &&
                                item.ApplicantId == applicantId && item.PlaidInstitutionId == objItem.PlaidInstitutionId),
                Builders<IItem>.Update.Set(a => a.Accounts, accounts)
            );

            return updateResult.IsAcknowledged;
        }

        public bool UpdateApplicantId(string inviteId, string applicantId)
        {
            var filter = Builders<IItem>
                                .Filter
                                .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                                && objItem.ApplicantId.Equals(inviteId));

            var result = Collection
                            .UpdateOne(filter,
                                       Builders<IItem>
                                        .Update
                                        .Set(objItem => objItem.ApplicantId, applicantId));
            return true;
        }


        /// <summary>
        /// Update Item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public void UpdateItem(IItem item)
        {
            var result = Collection
                            .UpdateOne(Builders<IItem>
                                        .Filter
                                        .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                                && objItem.Id.Equals(item.Id)),
                                       Builders<IItem>
                                        .Update
                                        .Set(objItem => objItem.PlaidItemId, item.PlaidItemId)
                                        .Set(objItem => objItem.PlaidInstitutionId, item.PlaidInstitutionId)
                                        .Set(objItem => objItem.PlaidPublicToken, item.PlaidPublicToken)
                                        .Set(objItem => objItem.PlaidAccessToken, item.PlaidAccessToken)
                                        .Set(objItem => objItem.Accounts, item.Accounts));

        }

        /// <summary>
        /// Update Account Balances
        /// </summary>
        /// <param name="applicantId">Applicant Id</param>
        /// <param name="accountId">Account Id</param>
        /// <param name="balance">Balances</param>
        /// <returns></returns>
        public bool UpdateAccountBalancesGivenAccountId(string applicantId, string accountId, Balances balance)
        {
            var builder = Builders<IItem>.Filter;
            var filter = builder.Eq("TenantId", TenantService.Current.Id);
            filter = filter & builder.Eq("Accounts.PlaidAccountId", accountId);
            var objItem = Collection
                            .Find(filter)
                            .FirstOrDefault<IItem>();

            if (objItem == null)
                throw new NotFoundException($"Item {objItem} cannot be found");

            //// locate the account using accountId and update the balances
            var accounts = objItem.Accounts;
            var accountToUpdate = accounts
                                    .Find(account => account.PlaidAccountId.Equals(accountId));

            if (accountToUpdate == null)
                throw new NotFoundException($"Account {accountToUpdate} not found");

            accountToUpdate.Balances = balance;

            //// Update information in database
            UpdateResult updateResult = Collection
                .UpdateOne
                (
                    Builders<IItem>
                        .Filter
                        .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                && item.ApplicantId.Equals(applicantId)
                                && item.PlaidInstitutionId.Equals(objItem.PlaidInstitutionId)),
                    Builders<IItem>
                        .Update
                        .Set(a => a.Accounts, accounts)
                );

            return updateResult.IsAcknowledged;
        }

        /// <summary>
        /// Get the list of access token after the specified no of days
        /// </summary>
        /// <param name="days">No Of days</param>
        /// <returns>list of access token</returns>
        public IList<string> GetAccessToken(int days)
        {
            DateTime checkDays = DateTime.Now.AddDays(days * (-1));
            var items = Query
                        .Where(item => !string.IsNullOrEmpty(item.PlaidAccessToken)
                               && item.CreatedDateTime < checkDays.ToUniversalTime()).ToList();

            var accessTokenList = items.SelectMany(x => new List<string>()
                {
                    x.PlaidAccessToken
                }).ToList();
            return accessTokenList;
        }

        /// <summary>
        /// UpdateAccessToNull
        /// </summary>
        /// <param name="accessTokens">accessTokens</param>
        /// <returns>true or false</returns>
        public void UpdateAccessToNull(List<string> accessTokens)
        {
            accessTokens.ForEach(at =>
                Collection
                     .UpdateOne(Builders<IItem>
                                 .Filter
                                 .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                         && objItem.PlaidAccessToken.Contains(at)),
                                 Builders<IItem>
                                 .Update
                                 .Set(objItem => objItem.PlaidAccessToken, null))
            );
        }
    }
}
