﻿using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.DocumentManager.Client;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Security.Encryption;

namespace Docitt.Syndication.Plaid.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittSyndicationPlaid"
                });
                   c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Syndication.Plaid.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddEncryptionHandler();

            services.AddTenantTime();

            services.AddTokenHandler();

            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddConfigurationService<Configuration>(
                Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
      
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            // document-manager
            services.AddDocumentManager();

            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // internals
            services.AddProjectDependencies();

            
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UsePlaidListener();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Syndication Plaid Service");
            });
            app.UseConfigurationCacheDependency();
        }
    }
}