﻿using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using System.Web;
//using RestSharp.Contrib;

namespace Docitt.Syndication.Plaid.Api.Controllers
{
    /// <summary>
    /// Institution Controller
    /// </summary>
    public class InstitutionsController : ExtendedController
    {
        /// <summary>
        /// InstitutionContoller Constructor
        /// </summary>
        /// <param name="institutionService">institution service object</param>
        /// <param name="log">logger</param>
        public InstitutionsController(            
            IInstitutionService institutionService,
            ILogger log)
        {
            this.InstitutionService = institutionService;
            this.log = log;
        }
           
        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Institution Service
        /// </summary>
        private IInstitutionService InstitutionService { get; }

        private ILogger log { get; }

        /// <summary>
        /// GetInstitutions
        /// </summary>
        /// <param name="count">count</param>
        /// <param name="offset">offset</param>
        /// <returns>IInstitutionsVM[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("institutions/get/{count}/{offset}")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionsVM[]), 200)]
#endif
        public Task<IActionResult> GetInstitutions(int count, int offset)
        {
            this.log.Info("Started GetInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsGetAsync(count, offset);
                this.log.Info("...Completed GetInstitutions");
                return this.Ok(result);
            });
        }

        /// <summary>
        /// GetInstitutions
        /// </summary>
        /// <param name="searchkey">searchkey</param>
        /// <param name="image">image</param>
        /// <returns>IInstitutionsSearchVM[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("institutions/search/{searchkey}")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionsSearchVM[]), 200)]
#endif
        public Task<IActionResult> GetInstitutions([FromRoute]string searchkey,[FromQuery]bool image)
        {
            this.log.Info("Started GetInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsSearchAsync(HttpUtility.UrlDecode(searchkey));
                this.log.Info("...Completed GetInstitutions");
                return this.Ok(result);
            });
        }

        /// <summary>
        /// GetInstitutionsWithDetails
        /// </summary>
        /// <param name="institutionId">institutionId</param>
        /// <returns>IInstitutionsVM</returns>
        [HttpPost]
        [HttpGet]
        [Route("institutions/detail/{institutionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionsVM), 200)]
#endif
        public Task<IActionResult> GetInstitutionsWithDetails(string institutionId)
        {
            this.log.Info("Started GetInstitutionsWithDetails...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsDetailsAsync(institutionId);
                this.log.Info("...Completed GetInstitutionsWithDetails");
                return Ok(result);
            });
        }

        /// <summary>
        /// GetSearchInstitutionList
        /// </summary>
        /// <returns>IInstitutionsSearchVM[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("institutions/search/list")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionsSearchVM[]), 200)]
#endif
        public Task<IActionResult> GetSearchInstitutionList()
        {
            this.log.Info("Started GetSearchInstitutionList...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsSearchListAsync();
                this.log.Info("...Completed GetSearchInstitutionList");
                return Ok(result);
            });
        }

        /// <summary>
        /// GetFeatureInstitutions
        /// </summary>
        /// <returns>IInstitutionsFeatureVM[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("institutions/feature")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionsFeatureVM[]), 200)]
#endif
        public Task<IActionResult> GetFeatureInstitutions()
        {
            this.log.Info("Started GetFeatureInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await InstitutionService.GetFeatureInstitutionsAsync();
                this.log.Info("...Completed GetFeatureInstitutions");
                return Ok(result);
            });
        }
    }
}
