﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Web;

namespace Docitt.Syndication.Plaid.Api.Controllers
{
    /// <summary>
    /// Item Controller
    /// </summary>
    public class ItemController : ExtendedController
    {
        /// <summary>
        /// Item Controller Constructor
        /// </summary>
        /// <param name="itemService">ItemService Object</param>
        /// <param name="logger">Logger Object</param>
        public ItemController(IItemService itemService, ILogger logger)
        {
            ItemService = itemService;
            Log = logger;
        }

        /// <summary>
        /// Get ItemService 
        /// </summary>
        private IItemService ItemService { get;}

        private ILogger Log { get; }

        /// <summary>
        /// Create Item using plaid link metadata. This is a new method that will replace old /item/create endpoint
        /// </summary>
        /// <param name="metaData">metadata from plaid link</param>
        /// <param name="entityId">entityId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("item/create/plaidlink/{entityId?}")]
        public Task<IActionResult> CreateItemGivenPlaidMetaData([FromBody]MetaData metaData, [FromRoute] string entityId)
        {
            //TODO: during family summary we need to get applicantId in request
            Log.Debug("Starting CreateItemGivenPlaidMetaData");
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.CreateItemGivenPlaidMetaData(metaData, entityId));
            });
        }

        /// <summary>
        /// Get the public keys and other configuration information for UI
        /// </summary>
        /// <returns>type of ResponsePlaidConfigs</returns>
        [HttpGet]
        [Route("publickey")]
        public Task<IActionResult> GetPlaidKeys()
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.GetPlaidKeys());
            });
        }

        //// Step 1 : for creating item and check the response if it is device type or selections or Direct
        /// <summary>
        /// CreateItem
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>IResponseItemCreate</returns>
        [HttpPost]
        [Route("item/create")]
        [ProducesResponseType(typeof(IResponseItemCreate), 200)]
        public Task<IActionResult> CreateItem([FromBody]RequestItemCreate request)
        {
            Log.Warn("This is deprecated, item/create/plaidlink should be used.");
            // Link will be taken taken using Plaid UI
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.CreateItemAsync(request));
            });
        }

        //// Update ApplicantId
        /// <summary>
        /// CreateItem
        /// </summary>
        /// <param name="inviteId">invite id</param>
        /// <param name="applicantId">applicant id</param>
        /// <returns>IResponseItemCreate</returns>
        [HttpPost]
        [Route("update/applicant/{applicantId}/invite/{inviteId}")]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> UpdateApplicantId(string inviteId, string applicantId)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.UpdateApplicantIdAsync(inviteId,applicantId));
            });
        }

        //// for creating item and check the response if it is device type or selections or Direct
        /// <summary>
        /// MfaRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns>IResponseItemCreate</returns>
        [HttpPost]
        [Route("item/mfa")]
        [ProducesResponseType(typeof(IResponseItemCreate), 200)]
        public Task<IActionResult> MfaRequest([FromBody]MFADocittRequest request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.ItemCreateMFAAsync(request));
            });
        }

        /// <summary>
        /// DeleteItem
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>PlaidResponseItemDelete[]</returns>
        [HttpPost]
        [Route("item/delete")]
        [ProducesResponseType(typeof(PlaidResponseItemDelete[]), 200)]
        public Task<IActionResult> DeleteItem([FromBody]RequestItemDelete request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.ItemDeleteAsync(request));
            });
        }

        /// <summary>
        /// DeleteItemAccount
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("item/account/delete")]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> DeleteItemAccount([FromBody]RequestItemDelete request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.ItemAccountDeleteAsync(request));
            });
        }

        /// <summary>
        /// GetItemAccount
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="entityId"></param>
        /// <returns>AccountsVM[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("item/accounts/all/{applicantId}/{entityId?}")]
        [ProducesResponseType(typeof(AccountsVM[]), 200)]
        public Task<IActionResult> GetItemAccount(string applicantId, string entityId = "")
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.GetItemsWithAccounts(applicantId.DocittUrlDecode(), entityId));
            });
        }

        /// <summary>
        /// UpdateAccountSync
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>IAccountSyncVM</returns>
        [HttpPut]
        [Route("item/accounts/sync")]
        [ProducesResponseType(typeof(IAccountSyncVM), 200)]
        public Task<IActionResult> UpdateAccountSync([FromBody]RequestItemAccount request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.GetAccountAsync(request));
            });
        }

        /// <summary>
        /// DelinkItems
        /// </summary>
        /// <param name="days">days</param>
        /// <returns>string[]</returns>
        [HttpPost]
        [Route("item/delink/{days}")]
        [ProducesResponseType(typeof(string[]), 200)]
        public Task<IActionResult> DelinkItems(int days)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.DelinkItems(days));
            });
        }

        /// <summary>
        /// UpdateItem
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>IResponseItemCreate</returns>
        [HttpPost]
        [Route("item/update")]
        [ProducesResponseType(typeof(IResponseItemCreate), 200)]
        public Task<IActionResult> UpdateItem([FromBody]RequestItemUpdate request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await ItemService.ItemUpdateAsync(request));
            });
        }
    }
}
