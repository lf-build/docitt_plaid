﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Api.Controllers
{
    /// <summary>
    /// Classs DataSyncController
    /// </summary>
    public class DataSyncController : ExtendedController
    {
        /// <summary>
        /// DataSyncController Constructor
        /// </summary>
        /// <param name="dataSyncService"></param>
        public DataSyncController(
            IDataSyncService dataSyncService)
        {
            this.DataSyncService = dataSyncService;
        }
       
        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Institution Service
        /// </summary>
        private IDataSyncService DataSyncService { get; }

        /// <summary>
        /// SyncInstitutions
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("syncinstitutions")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
#endif
        public Task<IActionResult> SyncInstitutions()
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.DataSyncService.SyncInstitutionsAsync());
            });
        }

        /// <summary>
        /// UpdateInstitutions
        /// </summary>
        /// <param name="institutionId">institutionId</param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("updateinstitutions")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
#endif
        public Task<IActionResult> UpdateInstitutions([FromQuery]string institutionId)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.DataSyncService.SyncInstitutionsByIdAsync(institutionId));
            });
        }        
    }
}
