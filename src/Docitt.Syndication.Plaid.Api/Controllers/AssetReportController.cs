using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
using RestSharp.Contrib;
#endif
using System.Threading.Tasks;
using System.Web;

namespace Docitt.Syndication.Plaid.Api.Controllers
{
    /// <summary>
    /// AssetReportController
    /// </summary>
    public class AssetReportController : ExtendedController
    {
        /// <summary>
        /// AssetController
        /// </summary>
        /// <param name="assetReportService">asset Report Service</param>
        /// <param name="logger">logger</param>
        public AssetReportController(IAssetReportService assetReportService, ILogger logger)
        {
            this.AssetReportService = assetReportService;
            this.Log = logger;
        }

        #region Private Variables
        /// <summary>
        /// Get AssetReportService 
        /// </summary>
        private IAssetReportService AssetReportService { get;}

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }
        #endregion

        /// <summary>
        /// CreateAssetReport
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("assetreport/create")]
        #if DOTNET2
        [ProducesResponseType(typeof(RequestAssetReportCreate), 200)]
        #endif
        public Task<IActionResult> CreateAssetReport([FromBody]RequestAssetReportCreate requestPayload)
        {
            Log.Debug("Started CreateAssetReport ...");
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.AssetReportService.AssetReportCreateAsync(requestPayload));
            });
        }

        /// <summary>
        /// CreateAssetReport
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("assetreport/get/data/{assetReportId}")]
        #if DOTNET2
        [ProducesResponseType(typeof(RequestAssetReportCreate), 200)]
        #endif
        public Task<IActionResult> AssetReportDataGet(string assetReportId)
        {
            Log.Debug("Started AssetReportDataGet ...");
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.AssetReportService.AssetReportGetDataAsync(assetReportId));
            });
        }

        /// <summary>
        /// AssetReportDataGetPDF
        /// </summary>
        [HttpGet]
        [Route("assetreport/get/pdf/{assetReportId}")]
        #if DOTNET2
        [ProducesResponseType(typeof(RequestAssetReportCreate), 200)]
        #endif
        public Task<IActionResult> AssetReportDataGetPDF(string assetReportId)
        {
            Log.Debug("Started AssetReportDataGetPDF ...");
            return ExecuteAsync(async () =>
            {
                var dataBytes = await this.AssetReportService.AssetReportGetPdf(assetReportId);
                var dataStream = new System.IO.MemoryStream(dataBytes);  
                var response = File(dataStream, "application/pdf");
                return response;
            });
        }

        /// <summary>
        /// CreateAssetReport
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("assetreport/remove")]
        #if DOTNET2
        [ProducesResponseType(typeof(RequestAssetReportCreate), 200)]
        #endif
        public Task<IActionResult> AssetReportRemove([FromBody]RequestAssetReportCreate requestPayload)
        {
            Log.Debug("Started AssetReportRemove ...");
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.AssetReportService.AssetReportRemoveAsync(requestPayload));
            });
        }
    }
}