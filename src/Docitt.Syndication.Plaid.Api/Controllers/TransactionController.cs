﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LendFoundry.Foundation.Services;
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid.Api.Controllers
{
    /// <summary>
    /// Transaction Controller Class
    /// </summary>
    public class TransactionController : ExtendedController
    {
        /// <summary>
        /// TransactionController
        /// </summary>
        /// <param name="transactionService">transaction Service</param>
        /// <param name="filterTransactionService">filter transaction service</param>
        public TransactionController(
            ITransactionService transactionService,
            IFilterTransactionService filterTransactionService
            )
        {
            TransactionService = transactionService;
            FilterTransactionService = filterTransactionService;
        }

        /// <summary>
        /// Gets TransactionService
        /// </summary>
        private ITransactionService TransactionService { get; }

        /// <summary>
        /// FilterTransactionService
        /// </summary>
        private IFilterTransactionService FilterTransactionService { get; }

        // Fetch from DB
        /// <summary>
        /// GetTransactionData
        /// </summary>
        /// <param name="request"></param>
        /// <param name="entityId"></param>
        /// <returns>ITransaction[]</returns>
        [HttpPost]
        [Route("transaction/get/{entityId?}")]
        [ProducesResponseType(typeof(ITransaction[]), 200)]
        public Task<IActionResult> GetTransactions([FromBody]RequestTransaction request, [FromRoute] string entityId)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await TransactionService.TransactionData(request, entityId));
            });
        }

        /// <summary>
        /// GetTransactionPull
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="entityId">entityId</param>
        /// <returns>TransactionResponse</returns>
        [HttpPost]
        [Route("transaction/pull/{entityId?}")]
        [ProducesResponseType(typeof(TransactionResponse), 200)]
        public Task<IActionResult> GetTransactionPull([FromBody]RequestTransaction request, [FromRoute] string entityId)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await TransactionService.PullTransactionAsync(request, entityId));
            });
        }

        /// <summary>
        /// GetTransactionDuration
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="entityId"></param>
        /// <returns>System.DateTime[]</returns>
        [HttpPost]
        [HttpGet]
        [Route("transaction/duration/{accountId}/{entityId?}")]
        [ProducesResponseType(typeof(System.DateTime[]), 200)]
        public Task<IActionResult> GetTransactionDuration(string accountId, string entityId)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await TransactionService.TransactionDurationDates(accountId, entityId));
            });
        }

        /// <summary>
        /// FilterTransaction
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>ITransaction[]</returns>
        [HttpPost]
        [Route("filtertransaction")]
        [ProducesResponseType(typeof(ITransaction[]), 200)]
        public Task<IActionResult> FilterTransaction([FromBody]FilterTransactionRequest request)
        {
            return ExecuteAsync(async () =>
            {
                return Ok(await FilterTransactionService.GetFilterTransactions(request));
            });
        }
    }
}
