﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Api
{
    /// <summary>
    /// OriginalNameContractResolver
    /// </summary>
    public class OriginalNameContractResolver : DefaultContractResolver
    {
        /// <summary>
        /// CreateProperties
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberSerialization"></param>
        /// <returns></returns>
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            // Let the base class create all the JsonProperties 
            IList<JsonProperty> list = base.CreateProperties(type, memberSerialization);

            // assign the C# property name
            foreach (JsonProperty prop in list)
            {
                prop.PropertyName = Char.ToLowerInvariant(prop.UnderlyingName[0]) + prop.UnderlyingName.Substring(1);
            }

            return list;
        }
    }
}
