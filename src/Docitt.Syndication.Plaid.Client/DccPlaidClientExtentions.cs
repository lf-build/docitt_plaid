﻿using System;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Syndication.Plaid.Client
{
    public static class DccPlaidServiceExtentions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddDccPlaidService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDccPlaidServiceFactory>(p => new DccPlaidServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDccPlaidServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDccPlaidService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IDccPlaidServiceFactory>(p => new DccPlaidServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IDccPlaidServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDccPlaidService(this IServiceCollection services)
        {
            services.AddTransient<IDccPlaidServiceFactory>(p => new DccPlaidServiceFactory(p));
            services.AddTransient(p => p.GetService<IDccPlaidServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}