﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Syndication.Plaid.Client
{ 
    public class DccPlaidServiceFactory : IDccPlaidServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public DccPlaidServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        } 
        
        public DccPlaidServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        
        private Uri Uri { get; }

        private IServiceProvider Provider { get; }

        
        public IDccPlaidService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("plaid");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new DccPlaidService(client);
        }
    }
}