﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using System.Web;

namespace Docitt.Syndication.Plaid.Client
{
    public class DccPlaidService : IDccPlaidService
    {
        public DccPlaidService(IServiceClient client)
        {
            this.Client = client;
        }

        private IServiceClient Client { get; }

        /// <summary>
        /// Delink Item with Plaid
        /// </summary>
        /// <param name="noOfDays">lookup days</param>
        /// <returns></returns>
        public async Task<string[]> DelinkItems(int noOfDays)
        {
            var request = new RestRequest($"item/delink/{noOfDays}", Method.POST);
            return await Client.ExecuteAsync<string[]>(request);
        }

        public async Task<List<Transaction>> GetFilterTransactions(
            DateTime startDate,
            DateTime endDate,
            IList<string> accountId,
            string applicantId,
            double threshold,
            List<string> categoryIds,
            string entityId = "")
        {
            var request = new RestRequest($"/filtertransaction", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            IFilterTransactionRequest requestData = new FilterTransactionRequest()
            {
                AccountId = accountId,
                ApplicantId = applicantId,
                StartDate = startDate,
                EndDate = endDate,
                TransactionType = "CR",
                TransactionAmount = threshold,
                PlaidCategoryIds = categoryIds,
                Comparer = ">",
                EntityId = entityId
            };

            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<List<Transaction>>(request);
        }

        #region Institutions

        /// <summary>
        /// Get the list of feature institutions
        /// </summary>
        /// <returns>list of institutions</returns>
        public async Task<List<IFeatureInstitutions>> GetFeatureInstitutions()
        {
            return await Client.GetAsync<List<IFeatureInstitutions>>($"institutions/feature");
        }

        /// <summary>
        /// Search Institutions
        /// </summary>
        /// <param name="searchkey">search key words</param>
        /// <returns>list of institutions</returns>
        public async Task<List<InstitutionsVM>> GetInstitutions(string searchkey)
        {
            string resource = string.Format($"institutions/search/{searchkey}");
            return await Client.GetAsync<List<InstitutionsVM>>(resource);
        }

        /// <summary>
        /// Get the list of institions
        /// </summary>
        /// <param name="count">no of records</param>
        /// <param name="offset">starting positions</param>
        /// <returns>list of institutions</returns>
        public async Task<List<InstitutionsVM>> GetInstitutions(int count, int offset)
        {
            string resource = string.Format($"institutions/get/{count}/{offset}");
            return await Client.GetAsync<List<InstitutionsVM>>(resource);
        }

        #endregion Institutions

        #region Item

        /// <summary>
        /// Get the list of feature institutions
        /// </summary>
        /// <returns>list of institutions</returns>
        public async Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId, string entityId = "")
        {
            var encodedApplicantId = HttpUtility.UrlEncode(applicantId);
            if (string.IsNullOrWhiteSpace(entityId))
            {
                return await Client.GetAsync<List<AccountsVM>>($"item/accounts/all/{encodedApplicantId}");
            }
            else
            {
                return await Client.GetAsync<List<AccountsVM>>($"item/accounts/all/{encodedApplicantId}/{entityId}");
            }
        }

        /// <summary>
        /// Create a Plaid Item using public access token
        /// </summary>
        /// <param name="metaData">metadata received from plaid after link</param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateItemGivenPlaidMetaData(MetaData metaData, string entityId = "")
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                return await Client.PostAsync<MetaData, ResponseItemCreate>($"item/create/plaidlink", metaData, true);
            }
            else
            {
                return await Client.PostAsync<MetaData, ResponseItemCreate>($"item/create/plaidlink/{entityId}", metaData, true);
            }

        }

        #endregion
    }
}
