using Docitt.Syndication.Plaid.Abstractions;
using Docitt.Syndication.Plaid.Persistence;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.DocumentManager.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.Syndication.Plaid
{
    public class AssetReportServiceFactory : IAssetReportServiceFactory
    {
        public AssetReportServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IAssetReportService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        { 
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Plaid.Abstractions.Settings.ServiceName, reader);
            var configuration = configurationService.Get();
            
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var itemRepository = repositoryFactory.CreateItemRepository(reader);
            var transactionRepository = repositoryFactory.CreateTransactionRepository(reader);
            var institutionRepository = repositoryFactory.CreateInstitutionsRepository(reader);

            var tokenReader = Provider.GetService<ITokenReader>();
            var tokenParser = Provider.GetService<ITokenHandler>();

            var plaidClientServiceFactory = Provider.GetService<IPlaidClientFactory>();
            var plaidClientService = plaidClientServiceFactory.Create(reader, handler, logger);

            var webhookRepository = repositoryFactory.CreateWebhookRepository(reader);
            var assetReportRepository = repositoryFactory.CreateAssetReportRepository(reader);

            var documentManagerServiceFactory = Provider.GetService<IDocumentManagerServiceFactory>();
            var documentManagerService = documentManagerServiceFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubFactory.Create(reader);

            return new AssetReportService(
                logger, 
                plaidClientService, 
                tenantTime,
                configuration, 
                tokenReader,
                tokenParser,
                itemRepository,
                assetReportRepository,
                eventHubService,
                documentManagerService
                );
        }
    }
}