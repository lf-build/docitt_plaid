using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.EventHub;
using LendFoundry.DocumentManager;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// AssetReportService
    /// </summary>
    public class AssetReportService : IAssetReportService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssetReportService"/> class.
        /// </summary>
        /// <param name="logger">logger</param>
        public AssetReportService(
            ILogger logger,
            IPlaidClient plaidClientService,
            ITenantTime tenantTime,
            IConfiguration configuration,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IItemRepository itemRepository,
            IAssetReportRepository assetReportRepository,
            IEventHubClient eventHubClient,
            IDocumentManagerService documentManagerService)
        {
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (configuration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");
            if (configuration.PlaidConfiguration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");
            if (documentManagerService == null) throw new ArgumentException($"{nameof(documentManagerService)} is mandatory");
            if (eventHubClient == null) throw new ArgumentException($"{nameof(eventHubClient)} is mandatory");

            this.TenantTime = tenantTime;
            this.Logger = logger;
            this.PlaidClientService = plaidClientService;
            this.Configuration = configuration;
            this.TokenReader = tokenReader;
            this.TokenParser = tokenParser;
            this.ItemRepository = itemRepository;
            this.AssetReportRepository = assetReportRepository;
            this.EventHub = eventHubClient;
            this.DocumentManagerService = documentManagerService;
        }

        #region Private Variables
        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Get TokenReader
        /// </summary>
        private ITokenReader TokenReader { get; }

        /// <summary>
        /// Token Parser
        /// </summary>
        private ITokenHandler TokenParser { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IPlaidClient PlaidClientService { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Logger
        /// </summary>
        /// <returns></returns>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets ItemRepository
        /// </summary>
        private IItemRepository ItemRepository { get; }

        /// <summary>
        /// Gets AssetReportRepository
        /// </summary>
        private IAssetReportRepository AssetReportRepository { get; }

        /// <summary>
        /// Gets EventHubClient
        /// </summary>
        private IEventHubClient EventHub { get; }

        /// <summary>
        /// Gets DocumentManagerService
        /// </summary>
        private IDocumentManagerService DocumentManagerService { get; }

        #endregion Private Variables

        /// <summary>
        /// AssetReportCreateAsync 
        /// </summary>
        /// <returns>response</returns>
        public async Task<IResponseAssetReportCreate> AssetReportCreateAsync(IRequestAssetReportCreate requestPayload)
        {
            Logger.Debug("AssetReportCreateAsync Started ...");
            IPlaidRequestAssetReportCreate requestData = new PlaidRequestAssetReportCreate();

            Logger.Debug($"... invoke GetApplicantAllAccessToken({requestPayload.ApplicantId})");
            var accessTokens = ItemRepository.GetApplicantAllAccessToken(requestPayload.ApplicantId);

            if (accessTokens == null || accessTokens.Count == 0)
                throw new Exception($"Item AccessToken not found for applicant {requestPayload.ApplicantId}");

            requestData.AccessTokens = accessTokens.Select(x => x.ToString()).ToArray();


            requestData.DaysRequested = this.Configuration.PlaidConfiguration.AssetReportRequestDays;
            requestData.ClientId = this.Configuration.PlaidConfiguration.ClientId;
            requestData.Secret = this.Configuration.PlaidConfiguration.Secret;
            requestData.Options = new AssetReportCreateOptions()
            {
                Webhook = this.Configuration.PlaidConfiguration.AssetReportWebhookUrl
                // TODO Options other fields
            };

            Logger.Debug("... calling PlaidClientService.AssetReportCreateAsync");
            var result = await PlaidClientService.AssetReportCreateAsync(requestData);

            if (result == null || result.HasError)
            {
                Logger.Debug($"... plaid returned error for applicant {requestPayload.ApplicantId}");
                return result;
            }

            Logger.Debug($"... response for applicant {requestPayload.ApplicantId} - AssetReporId '{result.AssetReportId}'");


            // Check if applicant already exists, if exist update the AssetReportId & AssetReportToken
            var assetReportData = await this.AssetReportRepository.GetAssetReportDataByApplicationId(requestPayload.ApplicantId);

            if (assetReportData != null)
            {
                Logger.Debug($"... updating information as user already exist");

                var oldAssetReportId = assetReportData.AssetReportId;
                var oldAssetReportToken = assetReportData.AssetReportToken;

                assetReportData.UpdatedDateTime = new TimeBucket(TenantTime.Now);
                assetReportData.AssetReportId = result.AssetReportId;
                assetReportData.AssetReportToken = result.AssetReportToken;
                this.AssetReportRepository.Update(assetReportData);

                //raise event
                await EventHub.Publish("PlaidAssetReportUpdated", new
                {
                    EntityId = requestPayload.ApplicantId,
                    OldAssetReportId = oldAssetReportId,
                    NewAssetReportId = result.AssetReportId
                });
            }
            else
            {
                Logger.Debug($"... adding");
                AssetReportRepository.Add(new AssetReport()
                {
                    ApplicantId = requestPayload.ApplicantId,
                    AssetReportId = result.AssetReportId,
                    AssetReportToken = result.AssetReportToken
                });

                await EventHub.Publish("PlaidAssetReportCreated", new
                {
                    EntityId = requestPayload.ApplicantId,
                    AssetReportId = result.AssetReportId,
                    AssetReportToken = result.AssetReportToken
                });
            }
            Logger.Debug($"... response stored in database");
            return result;
        }

        public async Task EventHandler_PRODUCT_READY(string assetReportId)
        {
            var data = await AssetReportGetDataAsync(assetReportId);
            var pdfData = await AssetReportGetPdf(assetReportId);
        }

        /// <summary>
        /// Generate AssetReport JSON
        /// </summary>
        /// <returns>response</returns>
        public async Task<IResponseAssetReportData> AssetReportGetDataAsync(string assetReportId)
        {
            Logger.Debug("AssetReportRespository.AssetReportGetDataAsync Started...");

            if (string.IsNullOrEmpty(assetReportId))
                throw new Exception("Missing AssetReportId");

            Logger.Debug($"...Get AssetReportToken from DataStore for the assetReportId {assetReportId}");
            var assetReport = await AssetReportRepository.GetAssetReportGivenAssetReportId(assetReportId);

            if (assetReport == null)
                throw new Exception("AssetReportToken not found");

            IPlaidRequestAssetReportData requestData = new PlaidRequestAssetReportData();

            requestData.AssetReportToken = assetReport.AssetReportToken;
            requestData.ClientId = this.Configuration.PlaidConfiguration.ClientId;
            requestData.Secret = this.Configuration.PlaidConfiguration.Secret;

            var result = await PlaidClientService.AssetReportGetDataAsync(requestData);
            Logger.Debug($"...response received from PlaidClientService.AssetReportGetDataAsync for the applicantId :{assetReport.ApplicantId} and assetReportId : {assetReportId} ");

            if (result == null || result.HasError)
            {
                Logger.Debug($"... plaid returned error for the applicant {assetReport.ApplicantId}");
                return result;
            }

            Logger.Debug("...invoke AssetReportRepository.UpdateAssetReportDataGivenAssetReportId");
            AssetReportRepository.UpdateAssetReportDataGivenAssetReportId(assetReport.AssetReportId, result);

            //Raise event 
            await EventHub.Publish("PlaidAssetReportDataReceived", new
            {
                EntityId = assetReport.ApplicantId,
                AssetReportId = assetReport.AssetReportId,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
            return result;
        }

        /// <summary>
        /// Generate AssetReport PDF
        /// </summary>
        /// <returns>response</returns>
        public async Task<byte[]> AssetReportGetPdf(string assetReportId)
        {
            Logger.Debug("AssetReportRespository.AssetReportGetPdf Started...");

            if (string.IsNullOrEmpty(assetReportId))
                throw new Exception("Missing AssetReportId");

            Logger.Debug($"Get AssetReportToken from DataStore for a applicantId {assetReportId}");
            var assetReport = await AssetReportRepository.GetAssetReportGivenAssetReportId(assetReportId);

            if (assetReport == null)
                throw new Exception("assetReport is null");

            IPlaidRequestAssetReportData requestData = new PlaidRequestAssetReportData();

            requestData.AssetReportToken = assetReport.AssetReportToken;
            requestData.ClientId = this.Configuration.PlaidConfiguration.ClientId;
            requestData.Secret = this.Configuration.PlaidConfiguration.Secret;

            var result = PlaidClientService.AssetReportGetPdf(requestData);
            Logger.Debug($"...response received from PlaidClientService.AssetReportGetPdf for the applicantId :{assetReport.ApplicantId} and assetReportId : {assetReportId} ");

            //Upload Document to DocumentManager
            var fileStream = new System.IO.MemoryStream(result);
            await DocumentManagerService.Create(fileStream, "plaid", assetReport.ApplicantId, "assetreport.pdf");

            Logger.Debug($"...Document has created for a applicantId {assetReportId}");
            //Raise event
            await EventHub.Publish("PlaidAssetReportPdfReceived", new
            {
                EntityId = assetReport.ApplicantId,
                AssetReportId = assetReport.AssetReportId
            });

            return result;
        }

        /// <summary>
        /// AssetReportRemoveAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IPlaidResponseAssetReportRemove> AssetReportRemoveAsync(IRequestAssetReportCreate requestPayload)
        {
            if (string.IsNullOrEmpty(requestPayload.ApplicantId))
                throw new Exception("Missing ApplicantId");

            Logger.Debug($"Get AssetReportToken from DataStore for a applicantId {requestPayload.ApplicantId}");
            var assetReportToken = await AssetReportRepository.GetAssetReportTokenGivenApplicationId(requestPayload.ApplicantId); ;

            if (string.IsNullOrEmpty(assetReportToken))
                throw new Exception("Asset Report token not found");

            Logger.Debug($"AssetReportToken found, now removing from Plaid");
            IPlaidRequestAssetReportData requestData = new PlaidRequestAssetReportData();
            requestData.AssetReportToken = assetReportToken;
            requestData.ClientId = this.Configuration.PlaidConfiguration.ClientId;
            requestData.Secret = this.Configuration.PlaidConfiguration.Secret;

            var result = await PlaidClientService.AssetReportRemove(requestData);

            Logger.Debug($"AssetReportToken removed from Plaid, now removing from DataStore");
            AssetReportRepository.DeleteAssetReportTokenGivenApplicationId(requestPayload.ApplicantId);

            if (result == null || result.HasError)
            {
                Logger.Debug($"... plaid returned error for applicant {requestPayload.ApplicantId}");
                return result;
            }

            Logger.Debug($"... plaid has deleted AssetReportToken for the applicant: {requestPayload.ApplicantId}");

            //Raise event
            await EventHub.Publish("PlaidAssetReportDelete", new
            {
                EntityId = requestPayload.ApplicantId,
                AssetReportToken = requestData.AssetReportToken,
                RequestId = result.RequestId
            });

            return result;
        }

    }
}