﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace  Docitt.Syndication.Plaid
{
    [Serializable]
    public class PlaidException : Exception
    {
        public PlaidException()
        {
        }

        public PlaidException(string message) : base(message)
        {
        }

        public PlaidException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PlaidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
