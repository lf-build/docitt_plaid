﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class DataSyncService : IDataSyncService
    {
        public DataSyncService
        (
            ILogger logger,
            IPlaidClient plaidClient,
            IConfiguration configuration,
            IInstitutionsRepository institutionRepository)
        {
            CommandExecutor = new CommandExecutor(logger);
            this.PlaidClientService = plaidClient;
            this.InstitutionRepository = institutionRepository;
            if (configuration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            Configuration = configuration;
            if (Configuration.PlaidConfiguration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            PlaidConfiguration = Configuration.PlaidConfiguration;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            CommandExecutor = new CommandExecutor(logger);
        }

        public const int CountUpperLimit = 500;
        public const int CountLowerLimit = 0;
        public const int OffsetLowerLimit = 0;

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IPlaidClient PlaidClientService { get; }

        private IPlaidConfiguration PlaidConfiguration { get; }

        private IInstitutionsRepository InstitutionRepository { get; }

        private IConfiguration Configuration { get; }

        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// SyncInstitutionsGetAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<bool> SyncInstitutionsAsync()
        {
            int count = 500, offset = 0;
            int totalInstitutions = await this.PlaidClientService.InstitutionsCountGetAsync(count, offset);

            for (offset = 0; offset < totalInstitutions; offset += 500)
            {
                if (totalInstitutions - offset < 500)
                {
                    count = totalInstitutions - offset;
                }

                var institutionResponse = await this.PlaidClientService.InstitutionsGetAsync(count, offset);

                var institutionResult = InstitutionMapper(institutionResponse);
                this.InstitutionRepository.AddInstitutions(institutionResult);
            }

            return true;
        }

        /// <summary>
        /// SyncInstitutionsGetAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<bool> SyncInstitutionsByIdAsync(string institutionId)
        {
            if (string.IsNullOrEmpty(institutionId))
            {
                var institutionResponse = await this.PlaidClientService.InstitutionsGetByIdAsync(institutionId);

                this.InstitutionRepository.UpdateInstitution(institutionResponse.institution);
            }
            else
            {
                IList<string> allInstitutionIds = this.InstitutionRepository.GetInstitutionIds();

                foreach (var insId in allInstitutionIds)
                {
                    var institutionResponse = await this.PlaidClientService.InstitutionsGetByIdAsync(insId);

                    this.InstitutionRepository.UpdateInstitution(institutionResponse.institution);
                }
            }

            return true;
        }

        private List<IInstitutions> InstitutionMapper(IList<InstitutionsVM> responses)
        {
            var institutions = new List<IInstitutions>();
            foreach (var response in responses)
            {
                var institution = new Institutions();
                institution.PlaidInstitutionId = response.PlaidInstitutionId;
                institution.InstitutionName = response.InstitutionName;
                institution.InstitutionLogo = response.InstitutionLogo;
                institution.HasMFA = response.HasMFA;
                institution.Products = response.Products;

                var credentials = new List<Credential>();
                response.credentials.ForEach(credential =>
                {
                    credentials.Add(new Credential()
                    {
                        Label = credential.Label,
                        Name = credential.Name,
                        Type = credential.Type
                    });
                });
                institution.Credentials = credentials;
                institution.CreatedDateTime = DateTime.Now;
                institutions.Add(institution);
            }

            return institutions;
        }        
    }
}