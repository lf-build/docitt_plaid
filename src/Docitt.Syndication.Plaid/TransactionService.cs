﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// GetTransactionService
    /// </summary>
    public class TransactionService : ITransactionService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionService"/> class.
        /// </summary>
        /// <param name="logger">logger</param>
        public TransactionService(ILogger logger, ITenantTime tenantTime,
            IConfiguration configuration,
            IPlaidClient plaidClient,
            ITransactionRepository transactionRepository,
            IItemRepository itemRepository)
        {
            TenantTime = tenantTime;
            CommandExecutor = new CommandExecutor(logger);
            TransactionRepository = transactionRepository;
            ItemRepository = itemRepository;
            PlaidClient = plaidClient;
            if (configuration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            Configuration = configuration;
            if (Configuration.PlaidConfiguration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            PlaidConfiguration = Configuration.PlaidConfiguration;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
        }

        private IPlaidConfiguration PlaidConfiguration { get; }

        private ITenantTime TenantTime { get; }
        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets TransactionRepository
        /// </summary>
        private ITransactionRepository TransactionRepository { get; }

        /// <summary>
        /// Gets ItemRepository
        /// </summary>
        private IItemRepository ItemRepository { get; }

        /// <summary>
        /// Gets PlaidClient
        /// </summary>
        private IPlaidClient PlaidClient { get; }

        //// Use for get transaction information , returns account information , item information and Transaction information.
        /// <summary>
        /// CreateItemAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<TransactionResponse> PullTransactionAsync(IRequestTransaction transactionRequest, string entityId = "")
        {
            var plaidTransactionRequest = CreatingTransactionModel(transactionRequest);
            var result = await PlaidClient.PullTransactionAsync(plaidTransactionRequest);
            /// TODO webhook done then need to updated
            TransactionRepository.AddTransactions(result.transactions, entityId);
            return result;
        }

        /// <summary>
        /// TransactionData
        /// </summary>
        /// <param name="transactionRequest">transactionRequest</param>
        /// <param name="entityId">entityId</param>
        /// <returns>response</returns>
        public async Task<IEnumerable<ITransaction>> TransactionData(IRequestTransaction transactionRequest, string entityId = "")
        {
            if (TransactionRequestValidate(transactionRequest))
            {
                var result = TransactionRepository.GetTransactions(transactionRequest, entityId);
                return await Task.Run(() => { return result; });
            }

            //// Return Invalid model
            throw new ArgumentException("Invalid model");
        }

        /// <summary>
        /// TransactionDurationDates
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>return dates intervals</returns>
        public async Task<IList<DateTime>> TransactionDurationDates(string accountId, string entityId = "")
        {
            var result = TransactionRepository.GetTransactionStartEndDates(accountId, entityId);
            return await Task.Run(() => { return result; });
        }

        /// <summary>
        /// GetAccessTokenByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>response</returns>
        public string GetAccessTokenByItemId(string itemId)
        {
            return ItemRepository.GetAccessTokenByItemId(itemId);
        }

        private bool TransactionRequestValidate(IRequestTransaction transactionRequest)
        {
            if (transactionRequest ==null  ||
                string.IsNullOrWhiteSpace(transactionRequest.StartDate) ||
                string.IsNullOrWhiteSpace(transactionRequest.EndDate) ||
                string.IsNullOrWhiteSpace(transactionRequest.PlaidAccountId) ||
                transactionRequest.Offset <= 0 || transactionRequest.Count < 0)
            {
                return false;
            }

            if (!IsValidRange(transactionRequest.StartDate, transactionRequest.EndDate))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// IsValidRange
        /// </summary>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <returns>response true or false</returns>
        private bool IsValidRange(string startdDay, string endDay)
        {
            var startDate = Convert.ToDateTime(startdDay);
            var endDate = Convert.ToDateTime(endDay);
            var todayDate = DateTime.Now;
            if (endDate.Date >= startDate.Date && endDate.Date <= todayDate.Date)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// CreatingTransactionModel
        /// </summary>
        /// <param name="transactionRequest">transactionRequest</param>
        /// <returns>response</returns>
        private PlaidRequestTransaction CreatingTransactionModel(IRequestTransaction transactionRequest)
        {
            var plaidTransactionRequest = new PlaidRequestTransaction
            {
                ClientId = PlaidConfiguration.ClientId,
                Secret = PlaidConfiguration.Secret,
                StartDate = transactionRequest.StartDate,
                EndDate = transactionRequest.EndDate,
                AccessToken = GetAccessTokenByItemId(transactionRequest.PlaidItemId),
                Options = new Options1
                {
                    Count = transactionRequest.Count,
                    Offset = transactionRequest.Offset
                }
            };

            return plaidTransactionRequest;
        }
    }
}
