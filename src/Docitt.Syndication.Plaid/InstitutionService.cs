﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class InstitutionService : IInstitutionService
    {
        public InstitutionService
        (
            ILogger logger,
            IPlaidClient plaidClient,
            ITenantTime tenantTime,
            IConfiguration configuration,
            IFeatureInstitutionsRepository featureInstitutionsRepository,
            IInstitutionsRepository institutionRepository)
        {
            TenantTime = tenantTime;
            this.PlaidClientService = plaidClient;
            this.FeatureInstitutionsRepository = featureInstitutionsRepository;
            this.InstitutionRepository = institutionRepository;

            if (configuration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            Configuration = configuration;
            if (Configuration.PlaidConfiguration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");

            PlaidConfiguration = Configuration.PlaidConfiguration;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            this.Log = logger;
        }

        public const int CountUpperLimit = 500;
        public const int CountLowerLimit = 0;
        public const int OffsetLowerLimit = 0;

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IPlaidClient PlaidClientService { get; }

        private IPlaidConfiguration PlaidConfiguration { get; }

        private ITenantTime TenantTime { get; }

        private IFeatureInstitutionsRepository FeatureInstitutionsRepository { get; }

        private IInstitutionsRepository InstitutionRepository { get; }

        private IConfiguration Configuration { get; }

        private ILogger Log { get; }

        /// <summary>
        /// GetInstitutionsAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<IList<InstitutionsVM>> InstitutionsGetAsync(int count, int offset)
        {
            // Offset minimum 1
            // Count minimum 0 and maximum 500
            if (count > CountLowerLimit && count <= CountUpperLimit && offset > OffsetLowerLimit)
            {
                var institutionResponse = await this.PlaidClientService.InstitutionsGetAsync(count, offset);
                return institutionResponse;
            }

            //// Return Invalid Argument
            throw new ArgumentException("Count or Offset value is not correct");
        }

        /// <summary>
        /// InstitutionsSearchAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<IList<InstitutionsSearchVM>> InstitutionsSearchAsync(string searchKey)
        {
            //// Return Invalid Argument
            if(string.IsNullOrWhiteSpace(searchKey))
            throw new ArgumentException("Please enter search value");

            var institutionResponse = this.InstitutionRepository.GetInstitutionsBySearch(searchKey);
            var result = new List<InstitutionsSearchVM>();

            foreach (var ins in institutionResponse)
            {
                var institution = new InstitutionsSearchVM()
                {
                    InstitutionName = ins.InstitutionName,
                    PlaidInstitutionId = ins.PlaidInstitutionId
                };
                result.Add(institution);
            }

            return await Task.Run(() => { return result; });
        }

        /// <summary>
        /// InstitutionsSearchAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<IList<InstitutionsSearchVM>> InstitutionsSearchListAsync()
        {
            var institutionResponse = this.InstitutionRepository.GetInstitutionsSearchList();

            var result = new List<InstitutionsSearchVM>();

            foreach (var ins in institutionResponse)
            {
                var institution = new InstitutionsSearchVM()
                {
                    InstitutionName = ins.InstitutionName,
                    PlaidInstitutionId = ins.PlaidInstitutionId
                };
                result.Add(institution);
            }

            return await Task.Run(() => { return result; });
        }

        /// <summary>
        /// InstitutionsSearchAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<IInstitutionsVM> InstitutionsDetailsAsync(string institutionId)
        {
            var institutionResponse = await this.InstitutionRepository.GetInstitutionByIdAsync(institutionId);
            return this.MapperInstitutionDetails(institutionResponse);
        }

        /// <summary>
        /// MapperInstitutionDetails
        /// </summary>
        /// <param name="institution">institution</param>
        /// <returns>response details</returns>
        private InstitutionsVM MapperInstitutionDetails(IInstitutions institution)
        {
            var result = new InstitutionsVM();
            result.credentials = institution.Credentials;
            result.HasMFA = institution.HasMFA;
            result.InstitutionLogo = institution.InstitutionLogo;
            result.InstitutionName = institution.InstitutionName;
            result.PlaidInstitutionId = institution.PlaidInstitutionId;
            result.Products = institution.Products;
            result.UrlAccountLocked = institution.UrlAccountLocked;
            result.UrlAccountSetup = institution.UrlAccountSetup;
            result.UrlForgottenPassword = institution.UrlForgottenPassword;
            result.Colors = institution.Colors;
            return result;
        }

        /// <summary>
        /// Get the list of Feature Institutions
        /// </summary>
        /// <returns>list of feature institutions</returns>
        public async Task<IList<InstitutionsFeatureVM>> GetFeatureInstitutionsAsync()
        {
            this.Log.Info("Started Service InstitutionsGetFeatureAsync...");

            this.Log.Info("Started Repository GetFeatureInstitutionsAsync..");
            var institutionsList = await this.FeatureInstitutionsRepository.GetFeatureInstitutionsAsync();
            this.Log.Info("...Completed Repository GetFeatureInstitutionsAsync");

            var result = new List<InstitutionsFeatureVM>();

            foreach (var ins in institutionsList)
            {
                var institution = new InstitutionsFeatureVM()
                {
                    InstitutionName = ins.InstitutionName,
                    InstitutionLogo = ins.InstitutionLogo,
                    PlaidInstitutionId = ins.PlaidInstitutionId
                };
                result.Add(institution);
            }
            this.Log.Info("...Completed Service GetFeatureInstitutionsAsync");
            return result;
        }
    }
}