﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// CreateItemService
    /// </summary>
    public class FilterTransactionService : IFilterTransactionService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemService"/> class.
        /// </summary>
        /// <param name="logger">logger</param>
        public FilterTransactionService(
            ILogger logger,
            ITransactionRepository transactionRepository,
            IItemRepository itemRepository)
        {
            CommandExecutor = new CommandExecutor(logger);
            TransactionRepository = transactionRepository;
            ItemRepository = itemRepository;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            CommandExecutor = new CommandExecutor(logger);
        }

        private ITransactionRepository TransactionRepository { get; }
        private IItemRepository ItemRepository { get; }
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// GetInstitutionsAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <returns></returns>
        public async Task<IList<ITransaction>> GetFilterTransactions(IFilterTransactionRequest request)
        {
            if (!string.IsNullOrWhiteSpace(request.ApplicantId))
            {
                request.AccountId = ItemRepository.GetAccountsByApplicantId(request.ApplicantId, request.EntityId);
            }

            var filterTransactionResponse = TransactionRepository.GetFilterTransactionData(request);

            return await Task.Run(() => { return filterTransactionResponse; });
        }
    }
}
