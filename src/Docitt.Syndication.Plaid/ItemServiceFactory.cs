﻿using Docitt.Syndication.Plaid.Abstractions;
using Docitt.Syndication.Plaid.Persistence;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.Syndication.Plaid
{
    public class ItemServiceFactory : IItemServiceFactory
    {
        public ItemServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IItemService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        { 
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var itemRepository = repositoryFactory.CreateItemRepository(reader);
            var transactionRepository = repositoryFactory.CreateTransactionRepository(reader);
            var institutionRepository = repositoryFactory.CreateInstitutionsRepository(reader);

            var tokenReader = Provider.GetService<ITokenReader>();
            var tokenParser = Provider.GetService<ITokenHandler>();

            var plaidClientServiceFactory = Provider.GetService<IPlaidClientFactory>();
            var plaidClientService = plaidClientServiceFactory.Create(reader, handler, logger);

            var webhookRepository = repositoryFactory.CreateWebhookRepository(reader);
            var plaidDelinkRepository = repositoryFactory.CreatePlaidDelinkRepository(reader);


            return new ItemService(logger, 
                plaidClientService, 
                tenantTime,
                configuration, 
                institutionRepository, 
                itemRepository, 
                transactionRepository, 
                webhookRepository, 
                plaidDelinkRepository,
                tokenReader, 
                tokenParser);
        }
    }
}