﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// CreateItemService
    /// </summary>
    public class ItemService : IItemService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemService"/> class.
        /// </summary>
        /// <param name="logger">logger</param>
        public ItemService(
            ILogger logger,
            IPlaidClient plaidClientService,
            ITenantTime tenantTime,
            IConfiguration configuration,
            IInstitutionsRepository institutionRepository,
            IItemRepository itemRepository,
            ITransactionRepository transactionRepository,
            IWebhookRepository webhookRepository,
            IPlaidDelinkRepository plaidDelinkRepository,
            ITokenReader tokenReader,
            ITokenHandler tokenParser)
        {
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (configuration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");
            if (configuration.PlaidConfiguration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");

            TenantTime = tenantTime;
            CommandExecutor = new CommandExecutor(logger);
            Logger = logger;
            PlaidClientService = plaidClientService;
            InstitutionRepository = institutionRepository;
            ItemRepository = itemRepository;
            TransactionRepository = transactionRepository;
            Configuration = configuration;
            PlaidConfiguration = Configuration.PlaidConfiguration;
            PlaidDelinkRepository = plaidDelinkRepository;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            WebhookRepository = webhookRepository;
        }

        /// <summary>
        /// PlaidDelinkRepository repository
        /// </summary>
        private IPlaidDelinkRepository PlaidDelinkRepository { get; }

        /// <summary>
        /// Get Plaid Configuration 
        /// </summary>
        private IPlaidConfiguration PlaidConfiguration { get; }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Get TokenReader
        /// </summary>
        private ITokenReader TokenReader { get; }

        /// <summary>
        /// Token Parser
        /// </summary>
        private ITokenHandler TokenParser { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IPlaidClient PlaidClientService { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets ItemRepository
        /// </summary>
        private IItemRepository ItemRepository { get; }

        private ILogger Logger { get; }

        /// <summary>
        /// Gets TransactionRepository
        /// </summary>
        private ITransactionRepository TransactionRepository { get; }

        /// <summary>
        /// Get InstitutionRepository
        /// </summary>
        private IInstitutionsRepository InstitutionRepository { get; }

        /// <summary>
        /// Gets WebhookRepository
        /// </summary>
        private IWebhookRepository WebhookRepository { get; }

        public async Task UpdateAccountBalancesAsync(string webhookCode, string itemId, string newTransactions)
        {
            Logger.Info("Currently it is triggered as : " + webhookCode);
            WebhookLog log = new WebhookLog()
            {
                ItemId = itemId,
                NewTransactions = newTransactions,
                WebhookCode = webhookCode,
                CreatedDate = TenantTime.Now
            };

            WebhookRepository.Add(log);

            IItem itemResult = null;

            if (webhookCode == "INITIAL_UPDATE" || webhookCode == "HISTORICAL_UPDATE")
            {
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    //// update accounts and balances
                    itemResult = ItemRepository.GetItemByGivenItemId(itemId);
                    if (itemResult == null)
                    {
                        throw new NotFoundException($"Item {itemId} not found");
                    }
                }, maxRetry: 5, sleepSeconds: 1);

                //// Plaid call for the new account balance sync
                //// update only balances for the accounts
                var updatedAccounts = await PlaidClientService.GetAccountAsync(itemResult.PlaidAccessToken);
                foreach (var account in itemResult.Accounts)
                {
                    var updatedAcc = updatedAccounts.Accounts.FirstOrDefault(x => x.PlaidAccountId == account.PlaidAccountId);
                    account.Balances = updatedAcc.Balances;
                    break;
                }

                ItemRepository.UpdateItem(itemResult);
                Logger.Info("Item account balances are updated");
                //// Add new transactions
                var plaidTransactionRequest = TransactionRequestCreate(itemResult.PlaidAccessToken, newTransactions);
                var result = await PlaidClientService.PullTransactionAsync(plaidTransactionRequest);
                Logger.Info($"Received transactions .. total_transactions : {result.total_transactions}");
                var transactionMessage = TransactionRepository.AddTransactions(result.transactions, itemResult.EntityId);
                Logger.Info($"TransactionRepository.AddTransactions response : {transactionMessage}");
            }
            ////throw new NotImplementedException();
        }

        public async Task<bool> UpdateApplicantIdAsync(string inviteId, string applicantId)
        {
            return ItemRepository.UpdateApplicantId(inviteId, applicantId);
        }

        //// Use for creating plaid item , returns account information , request Id and Public token.
        /// <summary>
        /// CreateItemAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateItemAsync(IRequestItemCreate requestData)
        {
            IResponseItemCreate response = new ResponseItemCreate();
            var itemStatus = ItemRepository.GetItemStatus(requestData.ApplicantId, requestData.InstitutionId, requestData.EntityId);

            switch (itemStatus)
            {
                case ItemStatus.AccountNotExists:
                    var resultDelink = await Delinking(requestData);
                    if (resultDelink.Deleted == true)
                    {
                        response = await PlaidLinking(requestData);
                    }
                    break;
                case ItemStatus.NoRecordsFound:
                    response = await PlaidLinking(requestData);
                    break;

                case ItemStatus.AccountExists:
                    response.HasError = true;
                    response.Error = new PlaidResponseError
                    {
                        DisplayMessage = "Item is already linked.",
                        ErrorMessage = "already linked",
                        ErrorCode = string.Empty,
                        ErrorType = string.Empty
                    };
                    break;
            }

            return response;
        }

        /// <summary>
        /// Create a plaid item from the metadata received from Plaid link
        /// </summary>
        /// <param name="metaData">metadata from plaid link</param>
        /// <param name="entityId">entityId</param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateItemGivenPlaidMetaData(IMetaData metaData, string entityId = "")
        {
            var applicantId = GetTokenUserName();
            Logger.Debug($"CreateItemGivenPlaidMetaData service for applicant {applicantId} and application {entityId}");

            if (metaData == null)
                throw new ArgumentException($"{nameof(metaData)} is mandatory", nameof(metaData));

            if (string.IsNullOrEmpty(metaData.PublicToken))
                throw new ArgumentException($"{nameof(metaData.PublicToken)} is mandatory", nameof(metaData.PublicToken));

            if (metaData.Institution == null)
                throw new ArgumentException($"{nameof(metaData.Institution)} is mandatory", nameof(metaData.Institution));

            if (string.IsNullOrEmpty(metaData.Institution.Id))
                throw new ArgumentException($"{nameof(metaData.Institution.Id)} is mandatory", nameof(metaData.Institution.Id));

            if (string.IsNullOrEmpty(metaData.Institution.Name))
                throw new ArgumentException($"{nameof(metaData.Institution.Id)} is mandatory", nameof(metaData.Institution.Name));

            Logger.Debug($"Exchange PublicToken for user {applicantId}");
            var accessTokenServiceResponse = await PlaidClientService.GetAccessTokenAsync(metaData.PublicToken);

            if (accessTokenServiceResponse == null)
                throw new PlaidException("Failed to exchange public token");

            if (string.IsNullOrEmpty(accessTokenServiceResponse.access_token))
                throw new PlaidException("Failed to exchange public token. Access token not received from plaid.");

            if (string.IsNullOrEmpty(accessTokenServiceResponse.item_id))
                throw new PlaidException("Failed to exchange public token. Item Id not received from plaid.");

            var accessToken = accessTokenServiceResponse.access_token;
            var itemId = accessTokenServiceResponse.item_id;

            // During Plaid Link PlaidAccountId is received as Id, so need to map
            foreach (var ac in metaData.Accounts)
            {
                if (string.IsNullOrEmpty(ac.PlaidAccountId) && !string.IsNullOrEmpty(ac.PlaidLinkAccountId))
                    ac.PlaidAccountId = ac.PlaidLinkAccountId;
            }

            Logger.Debug($"Create IResponseItemCreate object for applicant {applicantId}");
            IResponseItemCreate response = new ResponseItemCreate();
            response.IsLinked = true;
            response.HasError = false;
            response.HasMfa = false;
            response.InstitutionId = metaData.Institution.Id;
            response.Name = metaData.Institution.Name;
            response.PlaidLink = new PlaidLink()
            {
                PlaidItemId = itemId,
                RequestId = accessTokenServiceResponse.request_id,
                Accounts = metaData.Accounts,
                PublicToken = metaData.PublicToken,

            };
            //// Get the name and logo
            var institutionDetails = await InstitutionRepository.GetInstitutionByIdAsync(response.InstitutionId);
            if (institutionDetails != null)
            {
                response.Name = institutionDetails.InstitutionName;
                response.Logo = institutionDetails.InstitutionLogo;
            }

            //// check if item already exist - For update item
            var updateItem = ItemRepository.GetItemByGivenItemId(itemId);
            if (updateItem != null)
            {
                return UpdatedLinkedItem(response, updateItem);
            }

            Logger.Debug($"Get account owners information for user {applicantId} institution {metaData.Institution.Id} and application {entityId}");
            response = await GetIdentityAndAccounts(response, accessToken);

            Logger.Debug($"Create item object for user {applicantId} institution {metaData.Institution.Id} and application {entityId}");
            var itemResult = ItemMapperPlaidLink(response,
                                                      applicantId: applicantId,
                                                      institutionId: metaData.Institution.Id,
                                                      linkSessionId: metaData.LinkSessionId,
                                                      accessToken: accessToken,
                                                      itemId: itemId,
                                                      entityId: entityId);

            Logger.Debug($"AddItem for user {applicantId} institution {metaData.Institution.Id} and application {entityId}");
            ItemRepository.AddItem(itemResult);

            Logger.Debug($"Remove credit account information from the response not in DB for user {applicantId} institution {metaData.Institution.Id} and application {entityId}");
            response.PlaidLink.Accounts.RemoveAll(x => x.AccountType.Equals("credit"));

            return response;
        }

        public async Task<IResponsePlaidConfigs> GetPlaidKeys()
        {
            var response = new ResponsePlaidConfigs();
            response.PublicKeys = PlaidConfiguration.PublicKey;
            response.WebhookUrl = PlaidConfiguration.WebhookUrl;
            response.InitialProduct = PlaidConfiguration.InitialProducts;
            response.PlaidUIHeaderText = PlaidConfiguration.PlaidUIHeaderText;
            response.PlaidEnvironment = PlaidConfiguration.PlaidEnvironment;
            return await Task.Run(() => response);
        }

        /// <summary>
        /// PlaidLinking
        /// </summary>
        /// <param name="requestData">requestData</param>
        /// <returns>response for linked item</returns>
        private async Task<IResponseItemCreate> PlaidLinking(IRequestItemCreate requestData)
        {
            Logger.Info("Inside PlaidLinking... ");
            var response = await PlaidClientService.CreateItemExecutionFlow(requestData);

            //// Save accounts info to mongo db and further process...
            return await GenericLinkedItem(response, requestData);
        }

        /// <summary>
        /// MFAServiceAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> ItemCreateMFAAsync(IMFADocittRequest requestData)
        {
            var response = await PlaidClientService.CreateMfaExecutionFlow(requestData);
            //// Save accounts info to mongo db and further process...
            return await GenericLinkedItem(response, requestData);
        }

        /// <summary>
        /// Delete an account within Item linked on plaid. 
        /// </summary>
        /// <returns>response</returns>
        public async Task<bool> ItemAccountDeleteAsync(IRequestItemDelete requestParam)
        {
            return await Task.Run(() =>
            {
                return ItemRepository.RemoveAccountByAccountId(requestParam.ApplicantId, requestParam.PlaidAccountId, requestParam.EntityId);
            });
        }

        /// <summary>
        /// Delete an Item linked on plaid. 
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IRequestItemDelete requestParam)
        {
            //// Get list of access tokens given applicant id
            var accessTokens = ItemRepository.GetApplicantAllAccessToken(requestParam.ApplicantId, requestParam.EntityId);

            //// Delink from Plaid
            var response = await PlaidClientService.ItemDeleteAsync(accessTokens);

            //// Delete from  Database 
            foreach (var obj in response.Where(x => x.Deleted == true))
            {
                foreach (var at in accessTokens)
                {
                    if (obj.AccessToken == at.Substring(15, 8))
                    {
                        var item_id = ItemRepository.GetItemIdByAccessToken(at);
                        var accounts = ItemRepository.GetAccountsByItemId(item_id);
                        // Delete transaction information 
                        TransactionRepository.RemoveTransactionByAccount(accounts.ToList(), requestParam.EntityId);

                        ItemRepository.DeleteItemGivenAccessToken(at);
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId, string entityId = "")
        {
            var itemResult = ItemRepository.GetItemsByApplicantId(applicantId, entityId);
            return await MapperViewModel(itemResult);
        }

        /// <summary>
        /// GetAccountAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IAccountSyncVM> GetAccountAsync(IRequestItemAccount request)
        {
            IAccountSyncVM result = new AccountSyncVM();

            ////  Get Item detais from the ApplicantId and InstitutionId  as well as access token
            var itemData = ItemRepository.GetItemByAccountId(request.PlaidAccountId);

            if (itemData == null)
            {
                throw new InvalidArgumentException("Account is not exists");
            }
            else
            {
                //// Plaid call for the new account balance sync
                var updatedAccountsFromPlaid = await PlaidClientService.GetAccountAsync(itemData.PlaidAccessToken);

                //// update only balances for the accounts
                if (!updatedAccountsFromPlaid.HasError)
                {
                    var updatedAccount = updatedAccountsFromPlaid.Accounts.FirstOrDefault(x => x.PlaidAccountId == request.PlaidAccountId);
                    var itemResult = itemData.Accounts.FirstOrDefault(x => x.PlaidAccountId == request.PlaidAccountId);
                    itemResult.Balances = updatedAccount.Balances;
                    result.Account = itemResult;
                    var isUpdated = ItemRepository.UpdateAccountBalancesGivenAccountId(request.ApplicantId, request.PlaidAccountId, updatedAccount.Balances);
                }

                result.PlaidItemId = itemData.PlaidItemId;
                result.Error = updatedAccountsFromPlaid.Error;
                result.HasError = updatedAccountsFromPlaid.HasError;
            }
            return await Task.Run(() =>
            {
                return result;
            });
        }

        /// <summary>
        /// ItemUpdateAsync
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>Response Item Create</returns>
        public async Task<IResponseItemCreate> ItemUpdateAsync(IRequestItemUpdate request)
        {
            //// Get accesstoken from the Plaid Item Id
            var resultItem = ItemRepository.GetItemByGivenItemId(request.PlaidItemId);

            if (resultItem == null || string.IsNullOrWhiteSpace(resultItem.PlaidAccessToken))
            {
                throw new InvalidArgumentException("Item is not exist");
            }

            //// Creating public token new for the item
            var publicToken = await PlaidClientService.CreatingPublicTokenAsync(resultItem.PlaidAccessToken);

            if (string.IsNullOrWhiteSpace(publicToken))
            {
                throw new InvalidArgumentException("public token is not generated for the item , please try again");
            }

            //// update public token in DB - For the data correct.
            resultItem.PlaidPublicToken = publicToken;
            ItemRepository.UpdateItem(resultItem);

            /// return as it response from plaid , no updates in DB
            var response = await PlaidClientService.UpdateItemExecutionFlow(request, publicToken);

            return await GenericLinkedItem(response, request);
        }

        private async Task<IPlaidResponseItemDelete> Delinking(IRequestItemCreate requestData)
        {
            var itemData = ItemRepository.GetItemGivenApplicantIdInstitutionId(requestData.ApplicantId, requestData.InstitutionId, requestData.EntityId);
            IPlaidResponseItemDelete result = new PlaidResponseItemDelete();
            //// If item result not exists
            if (itemData == null)
            {
                result.Deleted = true;
                return result;
            }

            //// Remove linking from Plaid
            var responseList = await PlaidClientService.ItemDeleteAsync(new List<string>() { itemData.PlaidAccessToken });
            result = responseList.FirstOrDefault(); //// TODO - change once time
            if (result.Deleted == true)
            {
                //// Remove transactions Details 
                TransactionRepository.RemoveTransactionByAccount(itemData.Accounts.Select(x => x.PlaidAccountId).ToList<string>(), requestData.EntityId);
                //// Remove Item and Accounts Details
                ItemRepository.DeleteItemGivenItemId(itemData.PlaidItemId);
            }
            return result;
        }

        /// <summary>
        /// Delink Items
        /// </summary>
        /// <returns>return true or false</returns>
        public async Task<List<string>> DelinkItems(int days)
        {
            var delinkedAccessToken = new List<string>();
            var result = new List<string>();

            var accessTokenList = ItemRepository.GetAccessToken(days);


            if (accessTokenList != null && accessTokenList.Count() > 0)
            {
                foreach (var accesToken in accessTokenList)
                {
                    if (string.IsNullOrEmpty(accesToken))
                        continue;

                    //// delink item from Plaid
                    var response = await PlaidClientService.ItemDeleteNewAsync(accesToken);

                    delinkedAccessToken.Add(response.AccessToken);

                    IPlaidDelink data = new PlaidDelink()
                    {
                        AccessToken = accesToken,
                        Deleted = response.Deleted,
                        error = response.error,
                        CreatedDate = DateTime.UtcNow
                    };

                    PlaidDelinkRepository.Add(data);
                    result.Add(data.Id);

                }

                // update database
                ItemRepository.UpdateAccessToNull(delinkedAccessToken);
            }
            return result;
        }

        /// <summary>
        /// MapperViewModel
        /// </summary>
        /// <param name="resultItem">resultItem</param>
        /// <returns>response</returns>
        private async Task<IList<AccountsVM>> MapperViewModel(IList<IItem> resultItem)
        {
            var plaidItems = new List<AccountsVM>();
            foreach (var singleItem in resultItem)
            {
                var data = new AccountsVM();
                data.PlaidItemId = singleItem.PlaidItemId;
                data.InstitutionId = singleItem.PlaidInstitutionId;

                //// Get the name and logo
                var institutionDetails = await InstitutionRepository.GetInstitutionByIdAsync(data.InstitutionId);
                if (institutionDetails != null)
                {
                    data.Name = institutionDetails.InstitutionName;
                    data.Logo = institutionDetails.InstitutionLogo;
                }

                var accountList = new List<Accounts>();
                foreach (var singleAccount in singleItem.Accounts)
                {
                    accountList.Add(singleAccount);
                }
                data.Accounts = accountList;
                plaidItems.Add(data);
            }
            return plaidItems;
        }

        private IItem ItemMapper(IResponseItemCreate response, dynamic requestData, string accessToken, string itemId)
        {
            var item = new Item
            {
                Accounts = response.PlaidLink.Accounts,
                Identity = response.PlaidLink.Identity,
                ApplicantId = requestData.ApplicantId,
                PlaidAccessToken = accessToken,
                PlaidInstitutionId = requestData.InstitutionId,
                PlaidItemId = itemId,
                PlaidPublicToken = response.PlaidLink.PublicToken,
                CreatedDateTime = DateTime.Now
            };

            return item;
        }

        private IItem ItemMapperPlaidLink(IResponseItemCreate response,
                                          string applicantId,
                                          string institutionId,
                                          string accessToken,
                                          string linkSessionId,
                                          string itemId,
                                          string entityId = "")
        {
            var item = new Item
            {
                Accounts = response.PlaidLink.Accounts,
                Identity = response.PlaidLink.Identity,
                ApplicantId = applicantId,
                PlaidAccessToken = accessToken,
                PlaidInstitutionId = institutionId,
                PlaidItemId = itemId,
                LinkSessionId = linkSessionId,
                PlaidPublicToken = response.PlaidLink.PublicToken,
                CreatedDateTime = DateTime.Now,
                EntityId = entityId
            };

            return item;
        }

        /// <summary>
        /// TransactionRequestCreate
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <param name="newTransactions">newTransactions</param>
        /// <returns>return PlaidRequestTransaction</returns>
        private IPlaidRequestTransaction TransactionRequestCreate(string accessToken, string newTransactions)
        {
            return new PlaidRequestTransaction
            {
                AccessToken = accessToken,
                ClientId = PlaidConfiguration.ClientId,
                Secret = PlaidConfiguration.Secret,
                Options = new Options1
                {
                    Count = Convert.ToInt32(newTransactions),
                    Offset = 0
                },
                StartDate = CreatingStartDate(),
                EndDate = DateTime.Now.Date.ToString("yyyy-MM-dd"),

            };
        }

        /// <summary>
        /// CreatingStartDate
        /// </summary>
        /// <returns>return startdate</returns>
        private string CreatingStartDate()
        {
            return DateTime.Now.AddYears(-2).Date.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="requestData">requestData</param>
        /// <returns>Response</returns>
        private async Task<IResponseItemCreate> GenericLinkedItem(IResponseItemCreate response, dynamic requestData)
        {
            if (response.IsLinked == true && response.PlaidLink.PublicToken != null)
            {
                var accessTokenServiceResponse = await PlaidClientService.GetAccessTokenAsync(response.PlaidLink.PublicToken);
                var accessToken = accessTokenServiceResponse.access_token;
                response.PlaidLink.PlaidItemId = accessTokenServiceResponse.item_id;

                //// Get the name and logo
                var institutionDetails = await InstitutionRepository.GetInstitutionByIdAsync(response.InstitutionId);
                if (institutionDetails != null)
                {
                    response.Name = institutionDetails.InstitutionName;
                    response.Logo = institutionDetails.InstitutionLogo;
                }

                //// check if item already exist - For update item
                var updateItem = ItemRepository.GetItemByGivenItemId(response.PlaidLink.PlaidItemId);
                if (updateItem != null)
                {
                    return UpdatedLinkedItem(response, updateItem);
                }

                response = await GetIdentityAndAccounts(response, accessToken);
                var itemResult = ItemMapper(response, requestData, accessToken, response.PlaidLink.PlaidItemId);

                //// Insert into DB
                ItemRepository.AddItem(itemResult);

                //// Remove credit account information from the response not in DB
                response.PlaidLink.Accounts.RemoveAll(x => x.AccountType.Equals("credit"));
            }
            return response;
        }

        /// <summary>
        /// UpdatedLinkedItem
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="resultItem">resultItem</param>
        /// <returns>update response</returns>
        private IResponseItemCreate UpdatedLinkedItem(IResponseItemCreate response, IItem resultItem)
        {
            response.PlaidLink.Identity = resultItem.Identity;

            //// Remove islinked false and credit accounts
            resultItem.Accounts.RemoveAll(account => account.IsLinked == false || account.AccountType.Equals("credit"));
            var updatedAccounts = new List<Accounts>();

            foreach (var account in resultItem.Accounts)
            {
                Accounts updatedAccount = response.PlaidLink.Accounts.FirstOrDefault(x => x.PlaidAccountId == account.PlaidAccountId);
                if (updatedAccount != null)
                {
                    if (updatedAccount.Balances != null)
                    {
                        account.Balances = updatedAccount.Balances;
                        var acknowledgement = ItemRepository.UpdateAccountBalancesGivenAccountId(resultItem.ApplicantId, account.PlaidAccountId, account.Balances);
                    }
                    else
                    {
                        Logger.Debug($"Account {account.PlaidAccountId} with item {resultItem.PlaidInstitutionId} didn't returned balance");
                        account.Balances = new Balances();
                        account.Balances.Available = 0;
                        account.Balances.Current = 0;
                    }
                    updatedAccounts.Add(account);
                }
            }

            response.PlaidLink.Accounts = updatedAccounts;
            return response;
        }

        /// <summary>
        /// GetIdentityAndAccounts
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>IResponseItemCreate response</returns>
        private async Task<IResponseItemCreate> GetIdentityAndAccounts(IResponseItemCreate response, string accessToken)
        {
            //// Identity and account data
            var identityResponse = await PlaidClientService.GetIdentityAsync(accessToken);
            if (identityResponse != null && identityResponse.accounts != null)
            {
                response.PlaidLink.Identity = identityResponse.identity;
                response.PlaidLink.Accounts = identityResponse.accounts;
                foreach (var account in response.PlaidLink.Accounts)
                {
                    account.OwnerName = string.Join(",", identityResponse.identity.names.Select(x => " " + x));
                }
            }
            else
            {
                // These link doesn't return owner identity
                var updatedAccounts = await PlaidClientService.GetAccountAsync(accessToken);
                if (!updatedAccounts.HasError)
                {
                    foreach (var account in updatedAccounts.Accounts)
                    {
                        var updatedAcc = response.PlaidLink.Accounts.FirstOrDefault(x => x.PlaidAccountId == account.PlaidAccountId);
                        if (updatedAcc != null && account.Balances != null)
                        {
                            updatedAcc.Balances = account.Balances;
                        }
                    }
                }
            }

            //// Credit type linked false
            if (response.PlaidLink.Accounts != null)
            {
                response.PlaidLink.Accounts.Where(x => x.AccountType.Equals("credit")).ToList().ForEach(y => y.IsLinked = false);
            }
            return response;
        }

        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }
    }
}
