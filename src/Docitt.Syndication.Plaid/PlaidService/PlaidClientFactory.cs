﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid
{
    public class PlaidClientFactory : IPlaidClientFactory
    {
        public PlaidClientFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IPlaidClient Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();
            return new PlaidClient(logger, configuration);
        }
    }
}
