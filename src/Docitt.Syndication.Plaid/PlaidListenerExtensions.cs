﻿using Docitt.Syndication.Plaid.Abstractions;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace  Docitt.Syndication.Plaid
{
    public static class PlaidListenerExtensions
    {
        public static void UsePlaidListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IPlaidListener>().Start();
        }
    }
}
