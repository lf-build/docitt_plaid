namespace  Docitt.Syndication.Plaid
{
    public static class StringExtension
    {
        public static string DocittUrlDecode(this System.String str)
        {
            var decodeUrl = System.Net.WebUtility.UrlDecode(str);
            return decodeUrl.Replace(" ", "+");
        }
    }
}