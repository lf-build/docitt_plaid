﻿using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using LendFoundry.Foundation.Listener;
using System.Collections.Generic;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
namespace Docitt.Syndication.Plaid
{
    public class PlaidListener : ListenerBase, IPlaidListener
    {
        public PlaidListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IItemServiceFactory itemClientServiceFactory,
            IAssetReportServiceFactory assetReportServiceFactory
        ): base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {

            this.EventHubFactory = eventHubFactory;
            this.ConfigurationFactory = configurationFactory;
            this.TokenHandler = tokenHandler;
            this.TenantServiceFactory = tenantServiceFactory;
            this.Logger = loggerFactory.Create(NullLogContext.Instance);
            this.ItemClientServiceFactory = itemClientServiceFactory;
            this.AssetReportServiceFactory = assetReportServiceFactory;
            Logger.Debug($"Plaid Listner constructor");
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }

        private IItemServiceFactory ItemClientServiceFactory { get; }

         private IAssetReportServiceFactory AssetReportServiceFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.Events.Select(x=>x.Name).Distinct().ToList();
            }
        }
        
        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {                
                var token = TokenHandler.Issue(tenant, "PlaidIssuer");
                var reader = new StaticTokenReader(token.Value);
                var itemService = ItemClientServiceFactory.Create(reader, TokenHandler, Logger);
                var assetReportService = AssetReportServiceFactory.Create(reader, TokenHandler, Logger);

                var eventhub = EventHubFactory.Create(reader);
                // var lookupService = LookupServiceFactory.Create(reader);

                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    logger.Info($"#{configuration.Events.Count()} entity(ies) found from configuration: {Settings.ServiceName}");

                    var uniqueEvents = configuration.Events.Distinct().ToList();

                    uniqueEvents.ForEach(eventConfig =>
                    {
                        eventhub.On(eventConfig.Name, UpdateStatus(eventConfig, this.Logger, itemService, assetReportService));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
                    });

                    return uniqueEvents.Select(x=>x.Name).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }

        }

        private static Action<EventInfo> UpdateStatus(EventMapping eventConfiguration, ILogger logger, 
                                                    IItemService itemService, IAssetReportService assetReportService)
        {
            return @event =>
            {
                try
                {
                    logger.Info("eventName :");
                    var eventName = eventConfiguration.EventName.FormatWith(@event);
                    logger.Info($"{eventName}");

                    if (eventName == "plaidnotification")
                    {
                        var webhookType = eventConfiguration.WebhookType.FormatWith(@event);
                        var webhookCode = eventConfiguration.WebhookCode.FormatWith(@event);
                        var itemId = eventConfiguration.ItemId.FormatWith(@event);

                        logger.Info($"webhookType: {webhookType}; webhookCode: { webhookCode}; itemId: {itemId}");

                        if (webhookType == WebhookType.TRANSACTIONS.ToString())
                        {
                            var newTransactions = eventConfiguration.NewTransactions.FormatWith(@event);
                            logger.Info($"newTransactions: {newTransactions}");
                            itemService.UpdateAccountBalancesAsync(webhookCode, itemId, newTransactions);
                        }
                    }

                    if (eventName == "plaidassetreport")
                    {
                        var webhookType = eventConfiguration.WebhookType.FormatWith(@event);
                        var webhookCode = eventConfiguration.WebhookCode.FormatWith(@event);
                        
                        var assetReportId = eventConfiguration.AssetReportId.FormatWith(@event);

                        logger.Info($"webhookType: {webhookType}; webhookCode: { webhookCode}; itemId: {assetReportId}");

                        // TODO Move webhookCode & webhooktype to global enum or constant
                        if(webhookCode == "PRODUCT_READY" && webhookType == "ASSETS" 
                                  && assetReportId != null)
                            assetReportService.EventHandler_PRODUCT_READY(assetReportId);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }   
            };
        }
    }
}
