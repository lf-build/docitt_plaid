using Docitt.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.Syndication.Plaid
{
    public interface IAssetReportServiceFactory
    {
        IAssetReportService Create(
            ITokenReader reader, 
            ITokenHandler handler, 
            ILogger logger);
    }
}