﻿namespace Docitt.Syndication.Plaid
{
    public interface IRequestItemDelete
    {
        string ApplicantId { get; set; }

        string PlaidAccountId { get; set; }
        string EntityId { get; set; }
    }
}