﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface ICredential
    {
        ////string UserName { get; set; }
        ////string Password { get; set; }
        ////string Pin { get; set; }

        string Label { get; set; }
        string Name { get; set; }
        string Type { get; set; }
    }
}
