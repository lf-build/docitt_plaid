﻿namespace Docitt.Syndication.Plaid
{
    public enum ItemStatus
    {
        NoRecordsFound = -1,
        AccountExists = 1,
        AccountNotExists = 0
    }
}
