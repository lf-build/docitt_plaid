﻿using System.Collections.Generic;
using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid
{
    public class Configuration : IConfiguration
    {
        public EventMapping[] Events { get; set; }

        public PlaidConfiguration PlaidConfiguration { get; set; }

        public int CacheSlidingExpiration { get; set; }
        
        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
    }
}
