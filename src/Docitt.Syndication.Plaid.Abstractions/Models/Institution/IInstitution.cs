﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutions : IAggregate
    {
        string PlaidInstitutionId { get; set; }
        string InstitutionName { get; set; }
        string InstitutionLogo { get; set; }
        List<Credential> Credentials { get; set; }
        bool HasMFA { get; set; }
        string[] Products { get; set; }
        string UrlAccountLocked { get; set; }
        string UrlAccountSetup { get; set; }
        string UrlForgottenPassword { get; set; }
        Colors Colors { get; set; }

        DateTime CreatedDateTime { get; set; }
    }
}