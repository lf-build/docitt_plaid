﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class Colors
    {
        [JsonProperty("dark")]
        public string Dark { get; set; }

        [JsonProperty("darker")]
        public string Darker { get; set; }

        [JsonProperty("gradient")]
        public string[] Gradient { get; set; }

        [JsonProperty("primary")]
        public string Primary { get; set; }
    }
}