﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class Institutions : Aggregate, IInstitutions
    {
        [JsonProperty("institution_id")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("logo")]
        public string InstitutionLogo { get; set; }

        [JsonProperty("credentials")]
        public List<Credential> Credentials { get; set; }

        [JsonProperty("has_mfa")]
        public bool HasMFA { get; set; }

        [JsonProperty("products")]
        public string[] Products { get; set; }

        [JsonProperty("url_account_locked")]
        public string UrlAccountLocked { get; set; }

        [JsonProperty("url_account_setup")]
        public string UrlAccountSetup { get; set; }

        [JsonProperty("url_forgotten_password")]
        public string UrlForgottenPassword { get; set; }

        [JsonProperty("colors")]
        public Colors Colors { get; set; }
         
        public DateTime CreatedDateTime { get; set; }
    }
}