﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;

namespace Docitt.Syndication.Plaid
{
    public class Transaction : Aggregate, ITransaction
    {
        // public string AccountId { get; set; }

        [JsonProperty("account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("transaction_id")]
        public string PlaidTransactionId { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("category")]
        public string[] Category { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("transaction_type")]
        public string Type { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("pending")]
        public bool Pending { get; set; }

        [JsonProperty("name")]
        public string Description { get; set; }

        [JsonProperty("account_owner")]
        public string AccountOwner { get; set; }

        [JsonProperty("entity_id")]
        public string EntityId { get; set; }
    }
}