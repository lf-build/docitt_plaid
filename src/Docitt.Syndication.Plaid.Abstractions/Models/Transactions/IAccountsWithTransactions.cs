﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Syndication.Plaid
{
    public interface IAccountsWithTransactions
    {
        string AccountId { get; set; }

        Transaction Transactions { get; set; }
    }
}