﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Syndication.Plaid
{
    public interface ITransaction : IAggregate
    {
        // string AccountId { get; set; }
        string PlaidAccountId { get; set; }
        string PlaidTransactionId { get; set; }
        double Amount { get; set; }
        string[] Category { get; set; }
        string CategoryId { get; set; }
        string Type { get; set; }
        DateTime Date { get; set; }
        bool Pending { get; set; }
        string Description { get; set; }
        string AccountOwner { get; set; }
        string EntityId { get; set; }
    }
}