﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.Syndication.Plaid
{
    public class AccountsWithTransactions: IAccountsWithTransactions
    {
        public string AccountId { get; set; }

        public Transaction Transactions { get; set; }
    }
}