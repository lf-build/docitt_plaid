﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Syndication.Plaid
{
    public interface IWebhookLog : IAggregate
    {
        string Name { get; set; }

        string WebhookType { get; set; }

        string WebhookCode { get; set; }

        string ItemId { get; set; }

        string ErrorMessage { get; set; }

        string NewTransactions { get; set; }

        string RemovedTransactions { get; set; }

        bool IsSuccess { get; set; }

        DateTimeOffset CreatedDate { get; set; }
    }
}
