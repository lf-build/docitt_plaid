﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Syndication.Plaid
{
    public class WebhookLog : Aggregate, IWebhookLog
    {
        public string Name { get; set; }

        public string WebhookType { get; set; }

        public string WebhookCode { get; set; }

        public string ItemId { get; set; }

        public string ErrorMessage { get; set; }

        public string NewTransactions { get; set; }

        public string RemovedTransactions { get; set; }

        public bool IsSuccess { get; set; }

        public DateTimeOffset CreatedDate { get; set; }
    }
}
