using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// AssetReport Data Model
    /// </summary>
    public class AssetReport : Aggregate, IAssetReport
    {
        public AssetReport()
        {
            CreatedDateTime = new TimeBucket(DateTimeOffset.Now);
        }
        public string ApplicantId { get; set; }
        public string AssetReportId { get; set; }
        public string AssetReportToken { get; set; }
        public string RequestId { get; set; }

        public string Data {get; set;}
        public TimeBucket CreatedDateTime { get; set; }

        public TimeBucket UpdatedDateTime { get; set; }
    }    
}