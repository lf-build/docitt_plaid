using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// Asset Report Data Model
    /// </summary>
    public interface IAssetReport : IAggregate
    {
        string ApplicantId { get; set; }

        string AssetReportId { get; set; }

        string AssetReportToken { get; set; }

        string RequestId { get; set; }

        string Data {get; set;}
        
        TimeBucket CreatedDateTime { get; set; }

        TimeBucket UpdatedDateTime { get; set; }
    }
}