﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// Item Data Model
    /// </summary>
    public interface IItem : IAggregate
    {
        string ApplicantId { get; set; }
        string PlaidItemId { get; set; }
        string PlaidInstitutionId { get; set; }
        string PlaidPublicToken { get; set; }
        string PlaidAccessToken { get; set; }
        List<Accounts> Accounts { get; set; }
        string LinkSessionId {get; set;}
        DateTime CreatedDateTime { get; set; }
        Identity Identity { get; set; }
        string EntityId { get; set; }
    }    
}