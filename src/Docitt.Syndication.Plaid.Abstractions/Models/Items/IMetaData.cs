using System.Collections.Generic;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IMetaData
    {
        List<Accounts> Accounts {get; set;}
        Accounts Account {get; set;}
        string AccountId {get; set;}
        InstitutionMetaData Institution {get; set;}
        string PublicToken {get; set;}
        string LinkSessionId {get; set;}
    }
}