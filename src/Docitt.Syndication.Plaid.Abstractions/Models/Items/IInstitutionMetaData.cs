namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IInstitutionMetaData
    {
        string Id {get; set;}
        string Name {get; set;}
    }
}