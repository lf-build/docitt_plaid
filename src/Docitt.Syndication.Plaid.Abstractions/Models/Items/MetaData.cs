using System.Collections.Generic;
using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public class MetaData : IMetaData
    {
        [JsonProperty("accounts")]
        public List<Accounts> Accounts {get; set;}

        [JsonProperty("account")]
        public Accounts Account {get; set;}

        [JsonProperty("account_id")]
        public string AccountId {get; set;}

        [JsonProperty("institution")]
        public InstitutionMetaData Institution {get; set;}

        [JsonProperty("public_token")]
        public string PublicToken {get; set;}

        [JsonProperty("link_session_id")]
        public string LinkSessionId {get; set;}
    }
}

