﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// Plaid Delink Data Model
    /// </summary>
    public interface IPlaidDelink : IAggregate
    {
        string AccessToken { get; set; }

        bool Deleted { get; set; }

        IPlaidResponseError error { get; set; }

        DateTime CreatedDate { get; set; }
    }
}
