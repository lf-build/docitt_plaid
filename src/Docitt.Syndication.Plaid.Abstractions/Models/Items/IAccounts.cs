using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public interface IAccounts
    {
        string PlaidAccountId { get; set; }

        string PlaidLinkAccountId { get; set; }

        string AccountName { get; set; }

        string AccountType { get; set; }

        string Mask { get; set; }

        Balances Balances { get; set; }

        bool IsLinked { get; set; }
    }
}