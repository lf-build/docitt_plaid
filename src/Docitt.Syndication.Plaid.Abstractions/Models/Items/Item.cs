﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// Item Data Model
    /// </summary>
    public class Item : Aggregate, IItem
    {
        public string ApplicantId { get; set; }
        public string PlaidItemId { get; set; }
        public string PlaidInstitutionId { get; set; }
        public string PlaidPublicToken { get; set; }
        public string PlaidAccessToken { get; set; }
        public List<Accounts> Accounts { get; set; }
        public string LinkSessionId {get; set;}
        public DateTime CreatedDateTime { get; set; }
        public Identity Identity { get; set; }
        public string EntityId { get; set; }
    }    
}