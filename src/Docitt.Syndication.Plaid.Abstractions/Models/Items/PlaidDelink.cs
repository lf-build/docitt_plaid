﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// Plaid Delink Data Model
    /// </summary>
    public class PlaidDelink : Aggregate, IPlaidDelink
    {
        public string AccessToken { get; set; }

        public bool Deleted { get; set; }

        public IPlaidResponseError error { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
