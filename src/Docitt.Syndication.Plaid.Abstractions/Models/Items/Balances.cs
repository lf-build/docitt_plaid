﻿namespace Docitt.Syndication.Plaid
{
    public class Balances
    {
        public double? Available { get; set; }
        public double? Current { get; set; }
        public double? Limit { get; set; }
    }
}

