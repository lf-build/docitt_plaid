﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    /// <summary>
    /// Accounts Business Model
    /// </summary>
    public class Accounts : IAccounts
    {
        [JsonProperty("account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("id")]
        public string PlaidLinkAccountId { get; set; }

        [JsonProperty("name")]
        public string AccountName { get; set; }

        [JsonProperty("type")]
        public string AccountType { get; set; }

        [JsonProperty("mask")]
        public string Mask { get; set; }

        [JsonProperty("official_name")]
        public string OfficialName { get; set; }

        [JsonProperty("subtype")]
        public string SubType { get; set; }

        [JsonProperty("balances")]
        public Balances Balances { get; set; }

        public bool IsLinked { get; set; } = true;

        [JsonProperty("ownerName")]
        public string OwnerName { get; set; }
    }
}

