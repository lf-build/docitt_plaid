using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid.Abstractions
{
     public class InstitutionMetaData : IInstitutionMetaData
    {
        [JsonProperty("institution_id")]
        public string Id {get; set;}
        [JsonProperty("name")]
        public string Name {get; set;}
    }
}