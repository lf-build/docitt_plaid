﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class ResponseItemCreate : IResponseItemCreate
    {
        [JsonProperty("is_linked")]
        public bool IsLinked { get; set; }
        [JsonProperty("plaid_link")]
        public PlaidLink PlaidLink { get; set; }
        [JsonProperty("has_mfa")]
        public bool HasMfa { get; set; }
        [JsonProperty("mfa")]
        public PlaidMfa Mfa { get; set; }
        [JsonProperty("has_error")]
        public bool HasError { get; set; }
        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }
        [JsonProperty("institutionId")]
        public string InstitutionId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }
    }

    public class PlaidLink
    {
        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }

        [JsonProperty("accounts")]
        public List<Accounts> Accounts { get; set; }

        [JsonProperty("public_token")]
        public string PublicToken { get; set; }

        [JsonProperty("identity")]
        public Identity Identity { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }
    }

    public class PlaidMfa : IPlaidMfa
    {
        [JsonProperty("mfa_type")]
        public string MfaType { get; set; }
        [JsonProperty("device")]
        public DeviceViewModel Device { get; set; }
        [JsonProperty("device_list")]
        public DeviceListViewModel[] DeviceList { get; set; }
        [JsonProperty("selections")]
        public SelectionsViewModel[] Selections { get; set; }
        [JsonProperty("questions")]
        public string[] Questions { get; set; }
        [JsonProperty("public_token")]
        public string PublicToken { get; set; }
        [JsonProperty("request_id")]
        public string RequestId { get; set; }
    } 

   
}
