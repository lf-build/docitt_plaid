﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class FeatureInstitutionCredentials : IFeatureInstitutionCredentials
    {
        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
