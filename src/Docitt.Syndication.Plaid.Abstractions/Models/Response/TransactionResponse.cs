﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class TransactionResponse
    {
        public List<Accounts> accounts { get; set; }
        public ItemViewModel item { get; set; }
        public string request_id { get; set; }
        public int total_transactions { get; set; }
        public List<Transaction> transactions { get; set; }
    }
}
public class ItemViewModel
{
    public string[] available_products { get; set; }
    public string[] billed_products { get; set; }
    public object error { get; set; }
    public string institution_id { get; set; }
    public string item_id { get; set; }
    public string webhook { get; set; }
}

////public class Transaction
////{
////    public string account_id { get; set; }
////    public object account_owner { get; set; }
////    public float amount { get; set; }
////    public string[] category { get; set; }
////    public string category_id { get; set; }
////    public string date { get; set; }
////    public Location location { get; set; }
////    public string name { get; set; }
////    public Payment_Meta payment_meta { get; set; }
////    public bool pending { get; set; }
////    public object pending_transaction_id { get; set; }
////    public string transaction_id { get; set; }
////    public string transaction_type { get; set; }
////}

////public class Location
////{
////    public object address { get; set; }
////    public object city { get; set; }
////    public object lat { get; set; }
////    public object lon { get; set; }
////    public object state { get; set; }
////    public string store_number { get; set; }
////    public object zip { get; set; }
////}

////public class Payment_Meta
////{
////    public object by_order_of { get; set; }
////    public object payee { get; set; }
////    public object payer { get; set; }
////    public object payment_method { get; set; }
////    public object payment_processor { get; set; }
////    public object ppd_id { get; set; }
////    public object reason { get; set; }
////    public object reference_number { get; set; }
////}