﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionsFeatureVM
    {        
        string PlaidInstitutionId { get; set; }

        string InstitutionName { get; set; }

        string InstitutionLogo { get; set; }
    }

    public class InstitutionsFeatureVM : IInstitutionsFeatureVM
    {
        [JsonProperty("institutionId")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("logo", NullValueHandling = NullValueHandling.Ignore)]
        public string InstitutionLogo { get; set; }
    }
}
