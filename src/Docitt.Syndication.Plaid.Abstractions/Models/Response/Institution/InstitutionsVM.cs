﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionsVM
    {        
        List<Credential> credentials { get; set; }
               
        bool HasMFA { get; set; }
               
        string PlaidInstitutionId { get; set; }

        string[] mfa { get; set; }

        string InstitutionLogo { get; set; }

        string InstitutionName { get; set; }

        string[] Products { get; set; }

         string UrlAccountLocked { get; set; }

         string UrlAccountSetup { get; set; }

         string UrlForgottenPassword { get; set; }

         Colors Colors { get; set; }

    }

    public class InstitutionsVM : IInstitutionsVM
    {
        [JsonProperty("credentials")]
        public List<Credential> credentials { get; set; }

        [JsonProperty("has_mfa")]
        public bool HasMFA { get; set; }

        [JsonProperty("institutionId")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("mfa")]
        public string[] mfa { get; set; }

        [JsonProperty("logo", NullValueHandling = NullValueHandling.Ignore)]
        public string InstitutionLogo { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("products")]
        public string[] Products { get; set; }

        [JsonProperty("urlAccountLocked")]
        public string UrlAccountLocked { get; set; }

        [JsonProperty("urlAccountSetup")]
        public string UrlAccountSetup { get; set; }

        [JsonProperty("urlForgottenPassword")]
        public string UrlForgottenPassword { get; set; }

        [JsonProperty("colors")]
        public Colors Colors { get; set; }
    }
}
