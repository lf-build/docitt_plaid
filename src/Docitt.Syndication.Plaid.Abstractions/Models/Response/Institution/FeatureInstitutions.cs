﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class FeatureInstitutions : Aggregate, IFeatureInstitutions
    {
        [JsonProperty("credentials")]
        public List<FeatureInstitutionCredentials> Credentials { get; set; }

        [JsonProperty("has_mfa")]
        public bool HasMFA { get; set; }

        [JsonProperty("institution_id")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("image")]
        public string InstitutionLogo { get; set; }

        [JsonProperty("sort_order")]
        public int SortOrder { get; set; }
    }

    
}

