﻿using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class InstitutionsResponse : IInstitutionsResponse
    {
        public IList<InstitutionsVM> institutions { get; set; }

        public int total { get; set; }
    }

    public class InstitutionsUpdateResponse : IInstitutionsUpdateResponse
    {
        public Institutions institution { get; set; }
    }
}