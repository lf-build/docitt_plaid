﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionsSearchVM
    {        
        string PlaidInstitutionId { get; set; }

        string InstitutionName { get; set; }
    }

    public class InstitutionsSearchVM : IInstitutionsSearchVM
    {
        [JsonProperty("institutionId")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }
    }
}
