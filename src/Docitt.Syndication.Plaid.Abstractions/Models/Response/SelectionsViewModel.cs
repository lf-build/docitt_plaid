﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class SelectionsViewModel
    {
        public string[] answers { get; set; }
        public string question { get; set; }
    }
}
