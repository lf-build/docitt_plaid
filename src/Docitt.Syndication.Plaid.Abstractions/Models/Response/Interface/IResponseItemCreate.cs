﻿namespace Docitt.Syndication.Plaid
{
    public interface IResponseItemCreate
    {
        bool IsLinked { get; set; }

        PlaidLink PlaidLink { get; set; }

        bool HasMfa { get; set; }

        PlaidMfa Mfa { get; set; }

        bool HasError { get; set; }

        PlaidResponseError Error { get; set; }

        string InstitutionId { get; set; }

        string Name { get; set; }

        string Logo { get; set; }
    }
}