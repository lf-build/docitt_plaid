﻿namespace Docitt.Syndication.Plaid
{
    public interface IFeatureInstitutionCredentials
    {
        string Label { get; set; }
        string Name { get; set; }
        string Type { get; set; }
    }
}