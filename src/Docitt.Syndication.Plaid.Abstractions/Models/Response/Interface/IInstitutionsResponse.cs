﻿using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionsResponse
    {
        IList<InstitutionsVM> institutions { get; set; }

        int total { get; set; }
    }

    public interface IInstitutionsUpdateResponse
    {
        Institutions institution { get; set; }
    }
}