﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IFeatureInstitutions : IAggregate
    {
        List<FeatureInstitutionCredentials> Credentials { get; set; }
        bool HasMFA { get; set; }
        string InstitutionLogo { get; set; }
        string InstitutionName { get; set; }
        string PlaidInstitutionId { get; set; }
        int SortOrder { get; set; }
    }
}