using System.Collections.Generic;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IResponsePlaidConfigs
    {
        string PlaidUIHeaderText { get; set; }

        string PlaidEnvironment { get; set; }

        string PublicKeys { get; set; }

        string[] InitialProduct { get; set; }

        string WebhookUrl {get; set;}
    }
}