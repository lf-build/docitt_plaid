﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class MFAResponse
    {
        public object device { get; set; }
        public DeviceListViewModel[] device_list { get; set; }
        public string mfa_type { get; set; }
        public object accounts { get; set; }
        public string public_token { get; set; }
        public string request_id { get; set; }
        public string[] questions { get; set; }
        public SelectionsViewModel[] selections { get; set; }
    }
}



