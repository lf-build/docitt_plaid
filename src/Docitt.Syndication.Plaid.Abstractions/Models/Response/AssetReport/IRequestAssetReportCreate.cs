using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
     public interface IRequestAssetReportCreate
    {
       string ApplicantId {get; set;}
    }
}