using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestAssetReportData : IPlaidRequestAssetReportData
    {
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("asset_report_token")]
        public string AssetReportToken { get; set; }
    }
}