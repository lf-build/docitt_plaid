
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{

    public interface IPlaidRequestAssetReportData
    {
        string ClientId { get; set; }

        string Secret { get; set; }

        string AssetReportToken { get; set; }
    }
}