using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestAssetReportCreate : IPlaidRequestAssetReportCreate
    {
        [JsonProperty("client_id")]
        public string ClientId {get; set;}

        [JsonProperty("secret")]
        public string Secret {get; set;}
        
        [JsonProperty("access_tokens")]
        public string[] AccessTokens {get; set;}
    
        [JsonProperty("days_requested")]
        public int DaysRequested {get; set;}

        [JsonProperty("options")]    
        public AssetReportCreateOptions Options {get; set;}
    }
}