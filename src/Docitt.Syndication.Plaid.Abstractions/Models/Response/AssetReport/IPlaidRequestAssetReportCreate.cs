using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IPlaidRequestAssetReportCreate
    {
        string ClientId {get; set;}
        string Secret {get; set;}
        string[] AccessTokens {get; set;}
        int DaysRequested {get; set;}
        AssetReportCreateOptions Options {get; set;}
    }
}