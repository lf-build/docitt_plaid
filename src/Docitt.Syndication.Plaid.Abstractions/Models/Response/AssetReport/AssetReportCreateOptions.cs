using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class AssetReportCreateOptions
    {
        [JsonProperty("webhook")]    
        public string Webhook {get; set;}
    }
}