using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IResponseAssetReportCreate
    {
        string AssetReportId { get; set; }

        string AssetReportToken { get; set; }

        string RequestId { get; set; }

        bool HasError { get; set; }

        PlaidResponseError Error { get; set; }
    }

}