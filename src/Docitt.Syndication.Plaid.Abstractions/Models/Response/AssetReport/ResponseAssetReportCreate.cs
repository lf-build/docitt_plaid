using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class ResponseAssetReportCreate : IResponseAssetReportCreate
    {
        [JsonProperty("asset_report_id")]
        public string AssetReportId { get; set; }

        [JsonProperty("asset_report_token")]
        public string AssetReportToken { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }
    }
}