using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class PlaidResponseAssetReportRemove : IPlaidResponseAssetReportRemove
    {
        [JsonProperty("removed")]
        public bool Removed { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }
    }

}