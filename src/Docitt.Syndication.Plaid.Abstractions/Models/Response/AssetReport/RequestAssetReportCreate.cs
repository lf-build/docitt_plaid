using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public class RequestAssetReportCreate : IRequestAssetReportCreate
    {
        public string ApplicantId { get; set; }
    }

}