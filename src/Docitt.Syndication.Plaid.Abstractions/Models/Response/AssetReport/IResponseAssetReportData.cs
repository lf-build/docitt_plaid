using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IResponseAssetReportData
    {
        dynamic Report { get; set; }
        string RequestId { get; set; }
        bool HasError { get; set; }
        PlaidResponseError Error { get; set; }
    }
}