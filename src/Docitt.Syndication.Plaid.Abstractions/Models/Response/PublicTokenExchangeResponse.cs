﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class PublicTokenExchangeResponse
    {
        //[JsonProperty("access_token")]
        public string access_token { get; set; }

        //[JsonProperty("item_id")]
        public string item_id { get; set; }

        //[JsonProperty("request_id")]
        public string request_id { get; set; }
    }
}
