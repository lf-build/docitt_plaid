using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IAccountSyncVM
    {
        string PlaidItemId { get; set; }

        Accounts Account { get; set; }

        bool HasError { get; set; }

        PlaidResponseError Error { get; set; }
    }
}