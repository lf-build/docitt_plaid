﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{

    public class AccountSyncVM : IAccountSyncVM
    {
        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }

        [JsonProperty("account")]
        public Accounts Account { get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }
    }
}
