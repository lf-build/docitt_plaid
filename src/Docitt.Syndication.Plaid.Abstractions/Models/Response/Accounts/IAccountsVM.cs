using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IAccountsVM
    {
        string PlaidItemId { get; set; }

        string InstitutionId { get; set; }

        string Name { get; set; }

        string Logo { get; set; }

        List<Accounts> Accounts { get; set; }

         bool HasError { get; set; }

         PlaidResponseError Error { get; set; }
    }
}