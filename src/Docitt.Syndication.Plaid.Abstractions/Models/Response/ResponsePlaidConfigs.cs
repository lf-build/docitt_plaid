namespace Docitt.Syndication.Plaid.Abstractions
{
    public class ResponsePlaidConfigs : IResponsePlaidConfigs
    {
        public string PlaidUIHeaderText { get; set; }

        public string PlaidEnvironment { get; set; }

        public string PublicKeys { get; set; }

        public string[] InitialProduct { get; set; }

        public string WebhookUrl {get; set;}
    }
}