﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IResponseIdentity
    {
        List<Accounts> accounts { get; set; }
        Identity identity { get; set; }
        Item item { get; set; }
        string request_id { get; set; }
    }

    public class ResponseIdentity : IResponseIdentity
    {
        public List<Accounts> accounts { get; set; }
        public Identity identity { get; set; }
        public Item item { get; set; }
        public string request_id { get; set; }
    }

    public class Identity
    {
        public Address[] addresses { get; set; }
        public Email[] emails { get; set; }
        public string[] names { get; set; }
        public Phone_Numbers[] phone_numbers { get; set; }
    }

    public class Address
    {
        public string[] accounts { get; set; }
        public Data data { get; set; }
        public bool primary { get; set; }
    }

    public class Data
    {
        public string city { get; set; }
        public string state { get; set; }
        public string street { get; set; }
        public string zip { get; set; }
    }

    public class Email
    {
        public string data { get; set; }
        public bool primary { get; set; }
        public string type { get; set; }
    }

    public class Phone_Numbers
    {
        public string data { get; set; }
        public bool primary { get; set; }
        public string type { get; set; }
    }
}
