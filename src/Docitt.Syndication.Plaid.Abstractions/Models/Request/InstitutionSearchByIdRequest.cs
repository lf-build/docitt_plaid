using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class InstitutionSearchByIdRequest : IInstitutionSearchByIdRequest
    {
        public string institution_id { get; set; }
        public string public_key { get; set; }
        public SearchOptions options { get; set; }
    }
}