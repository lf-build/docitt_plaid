﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{

    public class MFARequest : IMFARequest
    {
        [JsonProperty("public_key")]
        public string Public_Key { get; set; }

        [JsonProperty("public_token")]
        public string Public_Token { get; set; }

        [JsonProperty("mfa_type")]
        public string Mfa_Type { get; set; }

        [JsonProperty("responses")]
        public string[] Responses { get; set; }
    }
}

