﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class RequestItemCreate : IRequestItemCreate
    {
        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("tenant_id")]
        public string TenantId { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }

        [JsonProperty("pin")]
        public string Pin { get; set; }

        [JsonProperty("entity_id")]
        public string EntityId { get; set; }
    }
}
