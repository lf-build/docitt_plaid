﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class MFADocittRequest : IMFADocittRequest
    {
        [JsonProperty("public_token")]
        public string PublicToken { get; set; }

        [JsonProperty("mfa_type")]
        public string MfaType { get; set; }

        [JsonProperty("responses")]
        public string[] Responses { get; set; }

        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }
    }
}
