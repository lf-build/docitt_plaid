﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class RequestTransaction : IRequestTransaction
    {
        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }
        
        [JsonProperty("plaid_account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("start_date")]
        public string StartDate { get; set; }

        [JsonProperty("end_date")]
        public string EndDate { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }
    }
}
