﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{

    
    public class InstitutionSearchRequest : IInstitutionSearchRequest
    {
        public string query { get; set; }

        public string[] products { get; set; }

        public string public_key { get; set; }

        public SearchOptions options { get; set; }
    }
}
