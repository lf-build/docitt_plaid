﻿namespace Docitt.Syndication.Plaid
{
    public interface IRequestItemUpdate
    {
        string ApplicantId { get; set; }

        string PlaidItemId { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        string InstitutionId { get; set; }

        string Pin { get; set; }
    }
}
