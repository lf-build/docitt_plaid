using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionSearchByIdRequest
    {
        string institution_id { get; set; }
        string public_key { get; set; }
        SearchOptions options { get; set; }        
    }
}