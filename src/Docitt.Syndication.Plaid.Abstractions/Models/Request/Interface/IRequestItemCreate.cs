﻿namespace Docitt.Syndication.Plaid
{
    public interface IRequestItemCreate
    {
        string ApplicantId { get; set; }

        string TenantId { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        string InstitutionId { get; set; }

        string Pin { get; set; }

        string EntityId { get; set; }
    }
}
