﻿namespace Docitt.Syndication.Plaid
{
    public  interface IRequestTransaction
    {
        string PlaidItemId { get; set; }

        string PlaidAccountId { get; set; }

        string StartDate { get; set; }

        string EndDate { get; set; }

        int Count { get; set; }

        int Offset { get; set; }
    }
}
