using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionRequest
    {
        string client_id { get; set; }
        string secret { get; set; }
        int count { get; set; }
        int offset { get; set; }
    }
}