﻿namespace Docitt.Syndication.Plaid
{
    public interface IRequestItemAccount
    {
         string ApplicantId { get; set; }

         string PlaidAccountId { get; set; }
    }
}
