using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IMFADocittRequest
    {
        string PublicToken { get; set; }

        string MfaType { get; set; }

        string[] Responses { get; set; }

        string ApplicantId { get; set; }

        string InstitutionId { get; set; }
    }
}