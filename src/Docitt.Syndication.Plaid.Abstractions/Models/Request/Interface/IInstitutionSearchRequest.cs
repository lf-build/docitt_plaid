using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IInstitutionSearchRequest
    {
        string query { get; set; }

        string[] products { get; set; }

        string public_key { get; set; }
    }
}