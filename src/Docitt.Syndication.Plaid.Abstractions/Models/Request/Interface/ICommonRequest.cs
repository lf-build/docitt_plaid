using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public interface ICommonRequest
    {
        string Client_Id { get; set; }

        string Secret { get; set; }

        string Access_Token { get; set; }
    }
}