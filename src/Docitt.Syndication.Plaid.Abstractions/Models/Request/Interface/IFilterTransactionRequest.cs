using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.Syndication.Plaid
{
    public interface IFilterTransactionRequest
    {
        IList<string> AccountId { get; set; }
        string ApplicantId { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        string TransactionType { get; set; } // CR or DR
        double TransactionAmount { get; set; }
        IList<string> PlaidCategoryIds { get; set; }
        string Comparer { get; set; }
        string EntityId { get; set; }
    }
}