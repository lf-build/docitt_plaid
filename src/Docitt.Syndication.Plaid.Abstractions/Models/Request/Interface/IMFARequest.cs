using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IMFARequest
    {
        string Public_Key { get; set; }

        string Public_Token { get; set; }

        string Mfa_Type { get; set; }

        string[] Responses { get; set; }
    }
}