﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class RequestItemUpdate : IRequestItemUpdate
    {
        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("pin")]
        public string Pin { get; set; }

        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }
    }
}
