﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class CommonRequest : ICommonRequest
    {
        [JsonProperty("client_id")]
        public string Client_Id { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("access_token")]
        public string Access_Token { get; set; }
    }
}