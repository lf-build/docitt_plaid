﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class InstitutionRequest : IInstitutionRequest
    {
        public string client_id { get; set; }
        public string secret { get; set; }
        public int count { get; set; }
        public int offset { get; set; }
    }
}
