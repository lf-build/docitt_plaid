﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class PlaidConnectRequest
    {
        public string BorrowerUserName { get; set; }

        public string BorrowerPassword { get; set; }

        public string InstitutionType { get; set; }
    }
}
