﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IInstitutionsRepository
    /// </summary>
    public interface IInstitutionsRepository : IRepository<IInstitutions>
    {
        /// <summary>
        /// GetInstitutions
        /// </summary>
        /// <returns>Institution Collection</returns>
        IList<IInstitutions> GetInstitutions();

        /// <summary>
        /// GetInstitutionsBySearch
        /// </summary>
        /// <param name="search">search</param>
        /// <returns>Institution search list</returns>
        IList<IInstitutions> GetInstitutionsBySearch(string search);

        /// <summary>
        /// GetInstitutionsSearchList
        /// </summary>
        /// <returns>Institution search list</returns>
        IList<IInstitutions> GetInstitutionsSearchList();

        /// <summary>
        /// GetInstitutionIds
        /// </summary>
        /// <returns>Institution ids Collection</returns>
        IList<string> GetInstitutionIds();

        /// <summary>
        /// GetInstitutionById
        /// </summary>
        /// <param name="institutionId">institutionId</param>
        /// <returns>Institutions</returns>
        Task<IInstitutions> GetInstitutionByIdAsync(string institutionId);

        /// <summary>
        /// AddInstitutions
        /// </summary>
        /// <param name="institutions">institutions</param>
        /// <returns>string</returns>
        string AddInstitutions(IEnumerable<IInstitutions> institutions);

        /// <summary>
        /// AddInstitution
        /// </summary>
        /// <param name="institution">institution</param>
        /// <returns>string</returns>
        string AddInstitution(IInstitutions institution);

        /// <summary>
        /// UpdateInstitution
        /// </summary>
        /// <param name="institution">institution</param>
        /// <returns>string</returns>
        string UpdateInstitution(IInstitutions institution);

        /// <summary>
        /// RemoveInstitutionByAccount
        /// </summary>
        /// <param name="institutionIds">institutionIds</param>
        /// <returns>bool</returns>
        bool RemoveInstitutionByInstitution(List<string> institutionIds);
    }
}