﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IFeatureInstitutionsRepository : IRepository<IFeatureInstitutions>
    {
        Task<List<IFeatureInstitutions>> GetFeatureInstitutionsAsync();
    }
}