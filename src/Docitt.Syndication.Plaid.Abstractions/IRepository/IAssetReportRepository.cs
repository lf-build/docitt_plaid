using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IAssetReportRepository
    /// </summary>
    public interface IAssetReportRepository : IRepository<IAssetReport>
    {
        Task<IAssetReport> GetAssetReportGivenAssetReportId(string assetReportId);

        Task<string> GetAssetReportTokenGivenApplicationId(string applicantId);

        Task<IAssetReport> GetAssetReportDataByApplicationId(string applicantId);

        bool DeleteAssetReportTokenGivenApplicationId(string applicantId);

        bool UpdateAssetReportDataGivenAssetReportId(string assetReportId, IResponseAssetReportData data);

        bool UpdateAssetReportDataGivenApplicantId(string applicantId, IResponseAssetReportData data);
    }
}