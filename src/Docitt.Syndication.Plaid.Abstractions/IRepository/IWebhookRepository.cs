﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IWebhookRepository :  IRepository<IWebhookLog>
    {
        bool AddWebhookLog(IWebhookLog webhookData);
    }
}
