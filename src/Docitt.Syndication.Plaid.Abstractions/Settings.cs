﻿using System;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "plaid";
    }
}
