﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class PlaidResponseItemDelete : IPlaidResponseItemDelete
    {
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        public PlaidResponseError error { get; set; }

        [JsonProperty("Id")]
        public string AccessToken { get; set; }
    }
}
