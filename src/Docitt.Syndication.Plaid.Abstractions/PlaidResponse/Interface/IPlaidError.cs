﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public interface IPlaidResponseError
    {
        string DisplayMessage { get; set; }
        string ErrorCode { get; set; }
        string ErrorMessage { get; set; }
        string ErrorType { get; set; }
        string RequestId { get; set; }
    }
}