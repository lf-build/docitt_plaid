﻿namespace Docitt.Syndication.Plaid
{
    public interface IPlaidResponseItemDelete
    {
        bool Deleted { get; set; }

        string RequestId { get; set; }

        PlaidResponseError error { get; set; }
        string AccessToken { get; set; }
    }
}