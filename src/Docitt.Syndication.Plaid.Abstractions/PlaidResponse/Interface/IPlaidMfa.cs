﻿namespace Docitt.Syndication.Plaid
{
    public interface IPlaidMfa
    {
        string MfaType { get; set; }
       
        DeviceViewModel Device { get; set; }
       
        DeviceListViewModel[] DeviceList { get; set; }
       
        SelectionsViewModel[] Selections { get; set; }
       
        string[] Questions { get; set; }
        
        string RequestId { get; set; }
    }
}