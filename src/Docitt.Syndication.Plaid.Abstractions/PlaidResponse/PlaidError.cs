﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class PlaidResponseError : IPlaidResponseError
    {
        [JsonProperty("display_message")]
        public string DisplayMessage { get; set; }
        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }
        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }
        [JsonProperty("error_type")]
        public string ErrorType { get; set; }
        [JsonProperty("request_id")]
        public string RequestId { get; set; }
    }
}
