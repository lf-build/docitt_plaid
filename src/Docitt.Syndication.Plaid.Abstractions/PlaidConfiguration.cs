﻿using Docitt.Syndication.Plaid.Abstractions;

namespace Docitt.Syndication.Plaid
{
    public class PlaidConfiguration : IPlaidConfiguration
    {
        public string ClientId { get; set; }

        public string Secret { get; set; }

        public string PublicKey { get; set; }

        public string UrlSandbox { get; set; }

        public string WebhookUrl { get; set; }

        public string AssetReportWebhookUrl {get; set;}

        public int AssetReportRequestDays { get; set; }

        public string[] InitialProducts { get; set; }

        public string PlaidUIHeaderText {get; set;}

        public string PlaidEnvironment {get; set;}
    }
}
