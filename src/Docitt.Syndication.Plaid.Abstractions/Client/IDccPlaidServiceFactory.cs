﻿using LendFoundry.Security.Tokens;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IDccPlaidServiceFactory
    {
        IDccPlaidService Create(ITokenReader reader);
    }
}
