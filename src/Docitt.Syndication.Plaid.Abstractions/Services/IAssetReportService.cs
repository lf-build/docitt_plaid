using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// Interface IAssetReportService
    /// </summary>
    public interface IAssetReportService
    {
        Task<IResponseAssetReportCreate> AssetReportCreateAsync(IRequestAssetReportCreate requestPayload);

        Task<IResponseAssetReportData> AssetReportGetDataAsync(string assetReportId);

        Task<byte[]> AssetReportGetPdf(string assetReportId);

        Task<IPlaidResponseAssetReportRemove> AssetReportRemoveAsync(IRequestAssetReportCreate requestPayload);

        Task EventHandler_PRODUCT_READY(string assetReportId);
    }

}