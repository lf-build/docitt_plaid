﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IFilterTransactionService
    {
        Task<IList<ITransaction>> GetFilterTransactions(IFilterTransactionRequest request);
    }
}
