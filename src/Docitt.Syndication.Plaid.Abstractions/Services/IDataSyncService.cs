﻿using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IDataSyncService
    {
        Task<bool> SyncInstitutionsAsync();

        Task<bool> SyncInstitutionsByIdAsync(string institutionId);
    }
}
