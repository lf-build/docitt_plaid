﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// ICreateItemService
    /// </summary>
    public interface IItemService
    {
        /// <summary>
        /// CreateItemProcessAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <param name="requestData">requestData</param>
        /// <returns></returns>
        Task<IResponseItemCreate> CreateItemAsync(IRequestItemCreate requestData);

        /// <summary>
        /// Create a new Item using public token received in metaData
        /// </summary>
        /// <param name="metaData">metadata received from plaid after UI link`</param>
        /// <returns></returns>
        Task<IResponseItemCreate> CreateItemGivenPlaidMetaData(IMetaData metaData, string entityId = "");

        Task<IResponsePlaidConfigs> GetPlaidKeys();

        Task<bool> UpdateApplicantIdAsync(string inviteId, string userName);

        Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IRequestItemDelete requestParam);

        Task<bool> ItemAccountDeleteAsync(IRequestItemDelete requestParam);

        Task<IResponseItemCreate> ItemCreateMFAAsync(IMFADocittRequest requestData);

        Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId, string entityId = "");

        Task<IAccountSyncVM> GetAccountAsync(IRequestItemAccount request);

        Task UpdateAccountBalancesAsync(string webhookCode, string itemId, string newTransactions);

        Task<List<string>> DelinkItems(int days);

        Task<IResponseItemCreate> ItemUpdateAsync(IRequestItemUpdate request);
    }
}
