﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IConfiguration : IDependencyConfiguration
    {
        EventMapping[] Events { get; set; }

        PlaidConfiguration PlaidConfiguration { get; set; }

        int CacheSlidingExpiration { get; set; }
    }
}
