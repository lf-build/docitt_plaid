﻿namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IPlaidConfiguration
    {
        string ClientId { get; set; }
        string PublicKey { get; set; }
        string Secret { get; set; }
        string UrlSandbox { get; set; }
        string WebhookUrl { get; set; }
        string AssetReportWebhookUrl {get; set;}
        int AssetReportRequestDays { get; set; }
        string[] InitialProducts { get; set; }
        string PlaidUIHeaderText {get; set;}
        string PlaidEnvironment {get; set;}
    }
}