﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IPlaidClient
    {
        Task<IResponseItemCreate> CreateItemExecutionFlow(IRequestItemCreate request);

        Task<IResponseItemCreate> CreateMfaExecutionFlow(IMFADocittRequest request);

        Task<IList<InstitutionsVM>> InstitutionsGetAsync(int count, int offset);

        Task<InstitutionsUpdateResponse> InstitutionsGetByIdAsync(string institutionId);

        Task<int> InstitutionsCountGetAsync(int count, int offset);

        Task<IList<InstitutionsVM>> InstitutionsSearchAsync(string institution);

        Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IEnumerable<string> accessTokens);

        Task<IPlaidResponseItemDelete> ItemDeleteNewAsync(string accessToken);

        Task<PublicTokenExchangeResponse> GetAccessTokenAsync(string public_token);

        Task<IAccountsVM> GetAccountAsync(string accessToken);

        Task<TransactionResponse> PullTransactionAsync(IPlaidRequestTransaction transactionRequest);

        Task<IResponseIdentity> GetIdentityAsync(string accessToken);

        Task<string> CreatingPublicTokenAsync(string accessToken);

        Task<IResponseItemCreate> UpdateItemExecutionFlow(IRequestItemUpdate requestData, string publicToken);
    
        Task<IResponseAssetReportCreate> AssetReportCreateAsync(IPlaidRequestAssetReportCreate requestData);
    
        Task<IResponseAssetReportData> AssetReportGetDataAsync(IPlaidRequestAssetReportData requestData);
    
        byte[] AssetReportGetPdf(IPlaidRequestAssetReportData requestData);
        
        Task<IPlaidResponseAssetReportRemove> AssetReportRemove(IPlaidRequestAssetReportData requestData);
    }
}
