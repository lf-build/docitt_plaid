﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IGetTransactionService
    /// </summary>
    public interface ITransactionService
    {
        Task<IEnumerable<ITransaction>> TransactionData(IRequestTransaction transactionRequest, string entityId = "");

        Task<TransactionResponse> PullTransactionAsync(IRequestTransaction transactionRequest, string entityId = "");

        Task<IList<DateTime>> TransactionDurationDates(string accountId, string entityId = "");
        
        string GetAccessTokenByItemId(string itemId);
    }
}
