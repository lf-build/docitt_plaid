﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.Syndication.Plaid.Abstractions
{
    public interface IPlaidClientFactory
    {
        IPlaidClient Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
