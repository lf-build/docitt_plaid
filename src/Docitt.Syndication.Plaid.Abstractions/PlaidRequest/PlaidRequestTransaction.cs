﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestTransaction : IPlaidRequestTransaction
    {
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        
        [JsonProperty("start_date")]
        public string StartDate { get; set; }

        [JsonProperty("end_date")]
        public string EndDate { get; set; }

        [JsonProperty("options")]
        public Options1 Options { get; set; }

        [JsonIgnore]
        public string EntityId { get; set; }

    }
}








