﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IPlaidRequestTransaction
    {
        string ClientId { get; set; }

        string Secret { get; set; }

        string AccessToken { get; set; }

        string StartDate { get; set; }

        string EndDate { get; set; }

        Options1 Options { get; set; }
    }
}
