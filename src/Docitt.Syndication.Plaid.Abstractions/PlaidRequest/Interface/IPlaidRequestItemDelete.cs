﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public interface IPlaidRequestItemDelete
    {
        string client_id { get; set; }
        string secret { get; set; }
        string access_token { get; set; }
    }
}
