﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestItemCreate : IPlaidRequestItemCreate
    {
        [JsonProperty("credentials")]
        public PlaidUserCredential Credentials { get; set; }

        [JsonProperty("institution_id")]
        public string Institution_Id { get; set; }

        [JsonProperty("public_key")]
        public string Public_Key { get; set; }

        [JsonProperty("options")]
        public Options Options { get; set; }

        [JsonProperty("initial_products")]
        public string[] Initial_Products { get; set; }
    }
}



