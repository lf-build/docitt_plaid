﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestItemDelete : IPlaidRequestItemDelete
    {
        [JsonProperty("client_id")]
        public string client_id { get; set; }
        [JsonProperty("secret")]
        public string secret { get; set; }
        [JsonProperty("access_token")]
        public string access_token { get; set; }
    }
}
