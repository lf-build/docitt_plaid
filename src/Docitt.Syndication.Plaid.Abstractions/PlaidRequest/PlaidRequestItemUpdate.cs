﻿using Newtonsoft.Json;

namespace Docitt.Syndication.Plaid
{
    public class PlaidRequestItemUpdate : IPlaidRequestItemUpdate
    {
        [JsonProperty("credentials")]
        public PlaidUserCredential Credentials { get; set; }

        [JsonProperty("public_token")]
        public string  PublicToken{ get; set; }

        [JsonProperty("public_key")]
        public string Public_Key { get; set; }
    }
}



