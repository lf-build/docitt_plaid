﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class EventMapping
    {
        public string Name { get; set; }

        public string EventName { get; set; }

        public string WebhookType { get; set; }

        public string WebhookCode { get; set; }

        public string ItemId { get; set; }

        public string Error { get; set; }

        public string NewTransactions { get; set; }

        public string RemovedTransactions { get; set; }

        public string AssetReportId {get; set;}
    }
}
