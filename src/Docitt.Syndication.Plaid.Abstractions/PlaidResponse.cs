﻿using Docitt.Syndication.Plaid.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Syndication.Plaid
{
    public class PlaidResponse
    {
        [JsonConverter(typeof(ConcreteListJsonConverter<List<PlaidAccount>, IPlaidAccount>))]
        [JsonProperty("accounts")]
        public List<PlaidAccount> accounts { get; set; }

        [JsonConverter(typeof(ConcreteListJsonConverter<List<PlaidTransaction>, IPlaidTransaction>))]
        [JsonProperty("transactions")]
        public IList<PlaidTransaction> transactions { get; set; }

        public string access_token { get; set; }
    }
    public interface IPlaidAccount
    {
         string _id { get; set; }
         string _item { get; set; }
         string _user { get; set; }
         AccountBalance balance { get; set; }
         string institution_type { get; set; }
         AccountMeta meta { get; set; }
         string subtype { get; set; }
         string type { get; set; }
    }

    public class PlaidAccount : IPlaidAccount
    {
        public string _id { get; set; }
        public string _item { get; set; }
        public string _user { get; set; }
        public AccountBalance balance { get; set; }
        public string institution_type { get; set; }
        public AccountMeta meta { get; set; }
        public string subtype { get; set; }
        public string type { get; set; }
    }

    public class AccountBalance
    {
        public float available { get; set; }
        public float current { get; set; }
    }

    public class AccountMeta
    {
        public string name { get; set; }
        public string number { get; set; }
    }

    public interface IPlaidTransaction
    {
         string _account { get; set; }
         string _id { get; set; }
         int amount { get; set; }
         string date { get; set; }
         string name { get; set; }
         TransactionMeta meta { get; set; }
         bool pending { get; set; }
         Type type { get; set; }
         string[] category { get; set; }
         string category_id { get; set; }
         TransactionScore score { get; set; }
    }

    public class PlaidTransaction : IPlaidTransaction
    {
        public string _account { get; set; }
        public string _id { get; set; }
        public int amount { get; set; }
        public string date { get; set; }
        public string name { get; set; }
        public TransactionMeta meta { get; set; }
        public bool pending { get; set; }
        public Type type { get; set; }
        public string[] category { get; set; }
        public string category_id { get; set; }
        public TransactionScore score { get; set; }
    }

    public class TransactionMeta
    {
        public TransactionLocation location { get; set; }
    }

    public class TransactionLocation
    {
        public string city { get; set; }
        public string state { get; set; }
    }

    public class TransactionType
    {
        public string primary { get; set; }
    }

    public class TransactionScore
    {
        public TransactionLocation1 location { get; set; }
        public int name { get; set; }
    }

    public class TransactionLocation1
    {
        public int city { get; set; }
        public int state { get; set; }
    }

}
